<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMoviesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('synopsis');
            $table->string('poster');
            $table->year('year');
            $table->string('duration');
            $table->boolean('active')->default('1');
            $table->boolean('has_movie_times')->default('0');
            $table->boolean('has_tickets')->default('0');
            //foreign keys
            $table->unsignedBigInteger('director_id');
            $table->unsignedBigInteger('distributor_id');
            $table->unsignedBigInteger('genre_id');
            $table->unsignedBigInteger('rating_id');
            //references
            $table->foreign('director_id')->references('id')->on('directors');
            $table->foreign('distributor_id')->references('id')->on('distributors');
            $table->foreign('genre_id')->references('id')->on('genres');
            $table->foreign('rating_id')->references('id')->on('ratings');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movies');
    }
}
