<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('price');
            $table->double('discount', 8,2);
            $table->double('total', 8,2);
            $table->string('employee_name');
            $table->dateTime('print_time');
            //foreign keys
            $table->unsignedBigInteger('movie_time_id');
            //references
            $table->foreign('movie_time_id')->references('id')->on('movie_times');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
