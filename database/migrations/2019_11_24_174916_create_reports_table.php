<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('ticket_amount');
            $table->bigInteger('tickets_2d');
            $table->bigInteger('tickets_3d');
            $table->decimal('copyright',10,2);
            $table->decimal('box_office',10,2);
            $table->decimal('iva_tax',10,2);
            $table->decimal('distributor_utilities');
            $table->decimal('film_exhibitor_utilities');
            $table->timestamps();
            //foreign keys
            $table->unsignedBigInteger('movie_id');
            //references
            $table->foreign('movie_id')->references('id')->on('movies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reports');
    }
}
