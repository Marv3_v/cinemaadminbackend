<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMovieTimesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movie_times', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('movie_time_date');
            $table->string('beginning_schedule');
            $table->integer('price');
            $table->double('discount', 8,2);
            $table->double('total', 8,2);
            $table->boolean('is_active')->default('1');
            $table->boolean('has_tickets')->default('0');
            //foreign keys
            $table->unsignedBigInteger('movie_id');
            $table->unsignedBigInteger('room_id');
            $table->unsignedBigInteger('display_type_id');
            //references
            $table->foreign('movie_id')->references('id')->on('movies');
            $table->foreign('room_id')->references('id')->on('rooms');
            $table->foreign('display_type_id')->references('id')->on('display_types');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movie_times');
    }
}
