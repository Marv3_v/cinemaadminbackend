<?php

use Illuminate\Database\Seeder;

class DistributorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $distributors = [
            [
                'name' => 'Warner Bros'
            ],
            [
                'name' => 'Sony Pictures'
            ],
            [
                'name' => 'Universal studios'
            ],
            [
                'name' => '20th Century Fox'
            ]
        ];

        DB::table('distributors')->insert($distributors);
    }
}
