<?php

use Illuminate\Database\Seeder;

class GenresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $genres = [
            [
                'genre' => 'Thriller'
            ],
            [
                'genre' => 'Comedy'
            ],
            [
                'genre' => 'Drama'
            ]
        ];

        DB::table('genres')->insert($genres);
    }
}
