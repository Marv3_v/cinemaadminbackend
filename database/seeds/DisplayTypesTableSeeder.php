<?php

use Illuminate\Database\Seeder;

class DisplayTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $displayTypes = [
            [
                'type' => '2D'
            ],
            [
                'type' => '3D'
            ]
        ];

        DB::table('display_types')->insert($displayTypes);
    }
}
