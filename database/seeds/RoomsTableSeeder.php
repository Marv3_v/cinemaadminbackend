<?php

use Illuminate\Database\Seeder;

class RoomsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rooms = [
            [
                'number' => 'Sala 1',
                'chairs_amount' => '300'
            ],
            [
                'number' => 'Sala 2',
                'chairs_amount' => '300'
            ],
            [
                'number' => 'Sala 3',
                'chairs_amount' => '300'
            ]
        ];

        DB::table('rooms')->insert($rooms);
    }
}
