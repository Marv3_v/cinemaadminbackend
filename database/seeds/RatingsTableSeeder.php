<?php

use Illuminate\Database\Seeder;

class RatingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ratings = [
            [
                'rating' => 'R'
            ],
            [
                'rating' => 'PG-13'
            ],
            [
                'rating' => 'G'
            ]
        ];

        DB::table('ratings')->insert($ratings);
    }
}
