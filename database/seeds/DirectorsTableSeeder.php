<?php

use Illuminate\Database\Seeder;

class DirectorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $directors = [
            [
                'fullname' => 'Edgar Wright'
            ],
            [
                'fullname' => 'Steven Spielberg'
            ],
            [
                'fullname' => 'Todd Phillips'
            ],
        ];

        DB::table('directors')->insert($directors);
        
    }
}
