<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [
                'role_code' => '1',
                'role_name' => 'admin'
            ],
            [
                'role_code' => '2',
                'role_name' => 'cajero'
            ]
        ];

        DB::table('roles')->insert($roles);
    }
}
