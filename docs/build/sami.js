
window.projectVersion = 'master';

(function(root) {

    var bhIndex = null;
    var rootPath = '';
    var treeHtml = '        <ul>                <li data-name="namespace:App" class="opened">                    <div style="padding-left:0px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="App.html">App</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="namespace:App_Console" class="opened">                    <div style="padding-left:18px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="App/Console.html">Console</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:App_Console_Kernel" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Console/Kernel.html">Kernel</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="namespace:App_Exceptions" class="opened">                    <div style="padding-left:18px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="App/Exceptions.html">Exceptions</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:App_Exceptions_Handler" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Exceptions/Handler.html">Handler</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="namespace:App_Http" class="opened">                    <div style="padding-left:18px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="App/Http.html">Http</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="namespace:App_Http_Controllers" >                    <div style="padding-left:36px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="App/Http/Controllers.html">Controllers</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="namespace:App_Http_Controllers_Auth" >                    <div style="padding-left:54px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="App/Http/Controllers/Auth.html">Auth</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:App_Http_Controllers_Auth_ConfirmPasswordController" >                    <div style="padding-left:80px" class="hd leaf">                        <a href="App/Http/Controllers/Auth/ConfirmPasswordController.html">ConfirmPasswordController</a>                    </div>                </li>                            <li data-name="class:App_Http_Controllers_Auth_ForgotPasswordController" >                    <div style="padding-left:80px" class="hd leaf">                        <a href="App/Http/Controllers/Auth/ForgotPasswordController.html">ForgotPasswordController</a>                    </div>                </li>                            <li data-name="class:App_Http_Controllers_Auth_LoginController" >                    <div style="padding-left:80px" class="hd leaf">                        <a href="App/Http/Controllers/Auth/LoginController.html">LoginController</a>                    </div>                </li>                            <li data-name="class:App_Http_Controllers_Auth_RegisterController" >                    <div style="padding-left:80px" class="hd leaf">                        <a href="App/Http/Controllers/Auth/RegisterController.html">RegisterController</a>                    </div>                </li>                            <li data-name="class:App_Http_Controllers_Auth_ResetPasswordController" >                    <div style="padding-left:80px" class="hd leaf">                        <a href="App/Http/Controllers/Auth/ResetPasswordController.html">ResetPasswordController</a>                    </div>                </li>                            <li data-name="class:App_Http_Controllers_Auth_VerificationController" >                    <div style="padding-left:80px" class="hd leaf">                        <a href="App/Http/Controllers/Auth/VerificationController.html">VerificationController</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="class:App_Http_Controllers_AdminController" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="App/Http/Controllers/AdminController.html">AdminController</a>                    </div>                </li>                            <li data-name="class:App_Http_Controllers_Controller" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="App/Http/Controllers/Controller.html">Controller</a>                    </div>                </li>                            <li data-name="class:App_Http_Controllers_DirectorController" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="App/Http/Controllers/DirectorController.html">DirectorController</a>                    </div>                </li>                            <li data-name="class:App_Http_Controllers_DisplayTypeController" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="App/Http/Controllers/DisplayTypeController.html">DisplayTypeController</a>                    </div>                </li>                            <li data-name="class:App_Http_Controllers_DistributorController" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="App/Http/Controllers/DistributorController.html">DistributorController</a>                    </div>                </li>                            <li data-name="class:App_Http_Controllers_GenreController" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="App/Http/Controllers/GenreController.html">GenreController</a>                    </div>                </li>                            <li data-name="class:App_Http_Controllers_HomeController" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="App/Http/Controllers/HomeController.html">HomeController</a>                    </div>                </li>                            <li data-name="class:App_Http_Controllers_MovieController" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="App/Http/Controllers/MovieController.html">MovieController</a>                    </div>                </li>                            <li data-name="class:App_Http_Controllers_MovieTimeController" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="App/Http/Controllers/MovieTimeController.html">MovieTimeController</a>                    </div>                </li>                            <li data-name="class:App_Http_Controllers_RatingController" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="App/Http/Controllers/RatingController.html">RatingController</a>                    </div>                </li>                            <li data-name="class:App_Http_Controllers_ReportController" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="App/Http/Controllers/ReportController.html">ReportController</a>                    </div>                </li>                            <li data-name="class:App_Http_Controllers_RoleController" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="App/Http/Controllers/RoleController.html">RoleController</a>                    </div>                </li>                            <li data-name="class:App_Http_Controllers_RoomController" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="App/Http/Controllers/RoomController.html">RoomController</a>                    </div>                </li>                            <li data-name="class:App_Http_Controllers_TicketController" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="App/Http/Controllers/TicketController.html">TicketController</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="namespace:App_Http_Middleware" >                    <div style="padding-left:36px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="App/Http/Middleware.html">Middleware</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:App_Http_Middleware_Authenticate" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="App/Http/Middleware/Authenticate.html">Authenticate</a>                    </div>                </li>                            <li data-name="class:App_Http_Middleware_CheckForMaintenanceMode" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="App/Http/Middleware/CheckForMaintenanceMode.html">CheckForMaintenanceMode</a>                    </div>                </li>                            <li data-name="class:App_Http_Middleware_Cors" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="App/Http/Middleware/Cors.html">Cors</a>                    </div>                </li>                            <li data-name="class:App_Http_Middleware_EncryptCookies" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="App/Http/Middleware/EncryptCookies.html">EncryptCookies</a>                    </div>                </li>                            <li data-name="class:App_Http_Middleware_RedirectIfAuthenticated" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="App/Http/Middleware/RedirectIfAuthenticated.html">RedirectIfAuthenticated</a>                    </div>                </li>                            <li data-name="class:App_Http_Middleware_TrimStrings" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="App/Http/Middleware/TrimStrings.html">TrimStrings</a>                    </div>                </li>                            <li data-name="class:App_Http_Middleware_TrustProxies" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="App/Http/Middleware/TrustProxies.html">TrustProxies</a>                    </div>                </li>                            <li data-name="class:App_Http_Middleware_VerifyCsrfToken" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="App/Http/Middleware/VerifyCsrfToken.html">VerifyCsrfToken</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="class:App_Http_Kernel" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Http/Kernel.html">Kernel</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="namespace:App_Providers" class="opened">                    <div style="padding-left:18px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="App/Providers.html">Providers</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:App_Providers_AppServiceProvider" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Providers/AppServiceProvider.html">AppServiceProvider</a>                    </div>                </li>                            <li data-name="class:App_Providers_AuthServiceProvider" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Providers/AuthServiceProvider.html">AuthServiceProvider</a>                    </div>                </li>                            <li data-name="class:App_Providers_BroadcastServiceProvider" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Providers/BroadcastServiceProvider.html">BroadcastServiceProvider</a>                    </div>                </li>                            <li data-name="class:App_Providers_EventServiceProvider" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Providers/EventServiceProvider.html">EventServiceProvider</a>                    </div>                </li>                            <li data-name="class:App_Providers_RouteServiceProvider" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Providers/RouteServiceProvider.html">RouteServiceProvider</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="class:App_Director" class="opened">                    <div style="padding-left:26px" class="hd leaf">                        <a href="App/Director.html">Director</a>                    </div>                </li>                            <li data-name="class:App_DisplayType" class="opened">                    <div style="padding-left:26px" class="hd leaf">                        <a href="App/DisplayType.html">DisplayType</a>                    </div>                </li>                            <li data-name="class:App_Distributor" class="opened">                    <div style="padding-left:26px" class="hd leaf">                        <a href="App/Distributor.html">Distributor</a>                    </div>                </li>                            <li data-name="class:App_Genre" class="opened">                    <div style="padding-left:26px" class="hd leaf">                        <a href="App/Genre.html">Genre</a>                    </div>                </li>                            <li data-name="class:App_Movie" class="opened">                    <div style="padding-left:26px" class="hd leaf">                        <a href="App/Movie.html">Movie</a>                    </div>                </li>                            <li data-name="class:App_MovieTime" class="opened">                    <div style="padding-left:26px" class="hd leaf">                        <a href="App/MovieTime.html">MovieTime</a>                    </div>                </li>                            <li data-name="class:App_Rating" class="opened">                    <div style="padding-left:26px" class="hd leaf">                        <a href="App/Rating.html">Rating</a>                    </div>                </li>                            <li data-name="class:App_Report" class="opened">                    <div style="padding-left:26px" class="hd leaf">                        <a href="App/Report.html">Report</a>                    </div>                </li>                            <li data-name="class:App_Role" class="opened">                    <div style="padding-left:26px" class="hd leaf">                        <a href="App/Role.html">Role</a>                    </div>                </li>                            <li data-name="class:App_Room" class="opened">                    <div style="padding-left:26px" class="hd leaf">                        <a href="App/Room.html">Room</a>                    </div>                </li>                            <li data-name="class:App_Ticket" class="opened">                    <div style="padding-left:26px" class="hd leaf">                        <a href="App/Ticket.html">Ticket</a>                    </div>                </li>                            <li data-name="class:App_User" class="opened">                    <div style="padding-left:26px" class="hd leaf">                        <a href="App/User.html">User</a>                    </div>                </li>                </ul></div>                </li>                </ul>';

    var searchTypeClasses = {
        'Namespace': 'label-default',
        'Class': 'label-info',
        'Interface': 'label-primary',
        'Trait': 'label-success',
        'Method': 'label-danger',
        '_': 'label-warning'
    };

    var searchIndex = [
                    
            {"type": "Namespace", "link": "App.html", "name": "App", "doc": "Namespace App"},{"type": "Namespace", "link": "App/Console.html", "name": "App\\Console", "doc": "Namespace App\\Console"},{"type": "Namespace", "link": "App/Exceptions.html", "name": "App\\Exceptions", "doc": "Namespace App\\Exceptions"},{"type": "Namespace", "link": "App/Http.html", "name": "App\\Http", "doc": "Namespace App\\Http"},{"type": "Namespace", "link": "App/Http/Controllers.html", "name": "App\\Http\\Controllers", "doc": "Namespace App\\Http\\Controllers"},{"type": "Namespace", "link": "App/Http/Controllers/Auth.html", "name": "App\\Http\\Controllers\\Auth", "doc": "Namespace App\\Http\\Controllers\\Auth"},{"type": "Namespace", "link": "App/Http/Middleware.html", "name": "App\\Http\\Middleware", "doc": "Namespace App\\Http\\Middleware"},{"type": "Namespace", "link": "App/Providers.html", "name": "App\\Providers", "doc": "Namespace App\\Providers"},
            
            {"type": "Class", "fromName": "App\\Console", "fromLink": "App/Console.html", "link": "App/Console/Kernel.html", "name": "App\\Console\\Kernel", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Console\\Kernel", "fromLink": "App/Console/Kernel.html", "link": "App/Console/Kernel.html#method_schedule", "name": "App\\Console\\Kernel::schedule", "doc": "&quot;Define the application&#039;s command schedule.&quot;"},
                    {"type": "Method", "fromName": "App\\Console\\Kernel", "fromLink": "App/Console/Kernel.html", "link": "App/Console/Kernel.html#method_commands", "name": "App\\Console\\Kernel::commands", "doc": "&quot;Register the commands for the application.&quot;"},
            
            {"type": "Class", "fromName": "App", "fromLink": "App.html", "link": "App/Director.html", "name": "App\\Director", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Director", "fromLink": "App/Director.html", "link": "App/Director.html#method_movies", "name": "App\\Director::movies", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Director", "fromLink": "App/Director.html", "link": "App/Director.html#method_getSearchResult", "name": "App\\Director::getSearchResult", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "App", "fromLink": "App.html", "link": "App/DisplayType.html", "name": "App\\DisplayType", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\DisplayType", "fromLink": "App/DisplayType.html", "link": "App/DisplayType.html#method_getSearchResult", "name": "App\\DisplayType::getSearchResult", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\DisplayType", "fromLink": "App/DisplayType.html", "link": "App/DisplayType.html#method_movieTime", "name": "App\\DisplayType::movieTime", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "App", "fromLink": "App.html", "link": "App/Distributor.html", "name": "App\\Distributor", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Distributor", "fromLink": "App/Distributor.html", "link": "App/Distributor.html#method_movies", "name": "App\\Distributor::movies", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Distributor", "fromLink": "App/Distributor.html", "link": "App/Distributor.html#method_getSearchResult", "name": "App\\Distributor::getSearchResult", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "App\\Exceptions", "fromLink": "App/Exceptions.html", "link": "App/Exceptions/Handler.html", "name": "App\\Exceptions\\Handler", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Exceptions\\Handler", "fromLink": "App/Exceptions/Handler.html", "link": "App/Exceptions/Handler.html#method_report", "name": "App\\Exceptions\\Handler::report", "doc": "&quot;Report or log an exception.&quot;"},
                    {"type": "Method", "fromName": "App\\Exceptions\\Handler", "fromLink": "App/Exceptions/Handler.html", "link": "App/Exceptions/Handler.html#method_render", "name": "App\\Exceptions\\Handler::render", "doc": "&quot;Render an exception into an HTTP response.&quot;"},
            
            {"type": "Class", "fromName": "App", "fromLink": "App.html", "link": "App/Genre.html", "name": "App\\Genre", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Genre", "fromLink": "App/Genre.html", "link": "App/Genre.html#method_movies", "name": "App\\Genre::movies", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Genre", "fromLink": "App/Genre.html", "link": "App/Genre.html#method_getSearchResult", "name": "App\\Genre::getSearchResult", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "App\\Http\\Controllers", "fromLink": "App/Http/Controllers.html", "link": "App/Http/Controllers/AdminController.html", "name": "App\\Http\\Controllers\\AdminController", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Http\\Controllers\\AdminController", "fromLink": "App/Http/Controllers/AdminController.html", "link": "App/Http/Controllers/AdminController.html#method_index", "name": "App\\Http\\Controllers\\AdminController::index", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "App\\Http\\Controllers\\Auth", "fromLink": "App/Http/Controllers/Auth.html", "link": "App/Http/Controllers/Auth/ConfirmPasswordController.html", "name": "App\\Http\\Controllers\\Auth\\ConfirmPasswordController", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Http\\Controllers\\Auth\\ConfirmPasswordController", "fromLink": "App/Http/Controllers/Auth/ConfirmPasswordController.html", "link": "App/Http/Controllers/Auth/ConfirmPasswordController.html#method___construct", "name": "App\\Http\\Controllers\\Auth\\ConfirmPasswordController::__construct", "doc": "&quot;Create a new controller instance.&quot;"},
            
            {"type": "Class", "fromName": "App\\Http\\Controllers\\Auth", "fromLink": "App/Http/Controllers/Auth.html", "link": "App/Http/Controllers/Auth/ForgotPasswordController.html", "name": "App\\Http\\Controllers\\Auth\\ForgotPasswordController", "doc": "&quot;&quot;"},
                    
            {"type": "Class", "fromName": "App\\Http\\Controllers\\Auth", "fromLink": "App/Http/Controllers/Auth.html", "link": "App/Http/Controllers/Auth/LoginController.html", "name": "App\\Http\\Controllers\\Auth\\LoginController", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Http\\Controllers\\Auth\\LoginController", "fromLink": "App/Http/Controllers/Auth/LoginController.html", "link": "App/Http/Controllers/Auth/LoginController.html#method___construct", "name": "App\\Http\\Controllers\\Auth\\LoginController::__construct", "doc": "&quot;Create a new controller instance.&quot;"},
            
            {"type": "Class", "fromName": "App\\Http\\Controllers\\Auth", "fromLink": "App/Http/Controllers/Auth.html", "link": "App/Http/Controllers/Auth/RegisterController.html", "name": "App\\Http\\Controllers\\Auth\\RegisterController", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Http\\Controllers\\Auth\\RegisterController", "fromLink": "App/Http/Controllers/Auth/RegisterController.html", "link": "App/Http/Controllers/Auth/RegisterController.html#method___construct", "name": "App\\Http\\Controllers\\Auth\\RegisterController::__construct", "doc": "&quot;Create a new controller instance.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\Auth\\RegisterController", "fromLink": "App/Http/Controllers/Auth/RegisterController.html", "link": "App/Http/Controllers/Auth/RegisterController.html#method_validator", "name": "App\\Http\\Controllers\\Auth\\RegisterController::validator", "doc": "&quot;Get a validator for an incoming registration request.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\Auth\\RegisterController", "fromLink": "App/Http/Controllers/Auth/RegisterController.html", "link": "App/Http/Controllers/Auth/RegisterController.html#method_create", "name": "App\\Http\\Controllers\\Auth\\RegisterController::create", "doc": "&quot;Create a new user instance after a valid registration.&quot;"},
            
            {"type": "Class", "fromName": "App\\Http\\Controllers\\Auth", "fromLink": "App/Http/Controllers/Auth.html", "link": "App/Http/Controllers/Auth/ResetPasswordController.html", "name": "App\\Http\\Controllers\\Auth\\ResetPasswordController", "doc": "&quot;&quot;"},
                    
            {"type": "Class", "fromName": "App\\Http\\Controllers\\Auth", "fromLink": "App/Http/Controllers/Auth.html", "link": "App/Http/Controllers/Auth/VerificationController.html", "name": "App\\Http\\Controllers\\Auth\\VerificationController", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Http\\Controllers\\Auth\\VerificationController", "fromLink": "App/Http/Controllers/Auth/VerificationController.html", "link": "App/Http/Controllers/Auth/VerificationController.html#method___construct", "name": "App\\Http\\Controllers\\Auth\\VerificationController::__construct", "doc": "&quot;Create a new controller instance.&quot;"},
            
            {"type": "Class", "fromName": "App\\Http\\Controllers", "fromLink": "App/Http/Controllers.html", "link": "App/Http/Controllers/Controller.html", "name": "App\\Http\\Controllers\\Controller", "doc": "&quot;&quot;"},
                    
            {"type": "Class", "fromName": "App\\Http\\Controllers", "fromLink": "App/Http/Controllers.html", "link": "App/Http/Controllers/DirectorController.html", "name": "App\\Http\\Controllers\\DirectorController", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Http\\Controllers\\DirectorController", "fromLink": "App/Http/Controllers/DirectorController.html", "link": "App/Http/Controllers/DirectorController.html#method_index", "name": "App\\Http\\Controllers\\DirectorController::index", "doc": "&quot;Display a listing of the resource.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\DirectorController", "fromLink": "App/Http/Controllers/DirectorController.html", "link": "App/Http/Controllers/DirectorController.html#method_create", "name": "App\\Http\\Controllers\\DirectorController::create", "doc": "&quot;Show the form for creating a new resource.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\DirectorController", "fromLink": "App/Http/Controllers/DirectorController.html", "link": "App/Http/Controllers/DirectorController.html#method_store", "name": "App\\Http\\Controllers\\DirectorController::store", "doc": "&quot;Store a newly created resource in storage.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\DirectorController", "fromLink": "App/Http/Controllers/DirectorController.html", "link": "App/Http/Controllers/DirectorController.html#method_show", "name": "App\\Http\\Controllers\\DirectorController::show", "doc": "&quot;Display the specified resource.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\DirectorController", "fromLink": "App/Http/Controllers/DirectorController.html", "link": "App/Http/Controllers/DirectorController.html#method_edit", "name": "App\\Http\\Controllers\\DirectorController::edit", "doc": "&quot;Show the form for editing the specified resource.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\DirectorController", "fromLink": "App/Http/Controllers/DirectorController.html", "link": "App/Http/Controllers/DirectorController.html#method_update", "name": "App\\Http\\Controllers\\DirectorController::update", "doc": "&quot;Update the specified resource in storage.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\DirectorController", "fromLink": "App/Http/Controllers/DirectorController.html", "link": "App/Http/Controllers/DirectorController.html#method_destroy", "name": "App\\Http\\Controllers\\DirectorController::destroy", "doc": "&quot;Remove the specified resource from storage.&quot;"},
            
            {"type": "Class", "fromName": "App\\Http\\Controllers", "fromLink": "App/Http/Controllers.html", "link": "App/Http/Controllers/DisplayTypeController.html", "name": "App\\Http\\Controllers\\DisplayTypeController", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Http\\Controllers\\DisplayTypeController", "fromLink": "App/Http/Controllers/DisplayTypeController.html", "link": "App/Http/Controllers/DisplayTypeController.html#method_index", "name": "App\\Http\\Controllers\\DisplayTypeController::index", "doc": "&quot;Display a listing of the resource.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\DisplayTypeController", "fromLink": "App/Http/Controllers/DisplayTypeController.html", "link": "App/Http/Controllers/DisplayTypeController.html#method_create", "name": "App\\Http\\Controllers\\DisplayTypeController::create", "doc": "&quot;Show the form for creating a new resource.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\DisplayTypeController", "fromLink": "App/Http/Controllers/DisplayTypeController.html", "link": "App/Http/Controllers/DisplayTypeController.html#method_store", "name": "App\\Http\\Controllers\\DisplayTypeController::store", "doc": "&quot;Store a newly created resource in storage.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\DisplayTypeController", "fromLink": "App/Http/Controllers/DisplayTypeController.html", "link": "App/Http/Controllers/DisplayTypeController.html#method_show", "name": "App\\Http\\Controllers\\DisplayTypeController::show", "doc": "&quot;Display the specified resource.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\DisplayTypeController", "fromLink": "App/Http/Controllers/DisplayTypeController.html", "link": "App/Http/Controllers/DisplayTypeController.html#method_edit", "name": "App\\Http\\Controllers\\DisplayTypeController::edit", "doc": "&quot;Show the form for editing the specified resource.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\DisplayTypeController", "fromLink": "App/Http/Controllers/DisplayTypeController.html", "link": "App/Http/Controllers/DisplayTypeController.html#method_update", "name": "App\\Http\\Controllers\\DisplayTypeController::update", "doc": "&quot;Update the specified resource in storage.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\DisplayTypeController", "fromLink": "App/Http/Controllers/DisplayTypeController.html", "link": "App/Http/Controllers/DisplayTypeController.html#method_destroy", "name": "App\\Http\\Controllers\\DisplayTypeController::destroy", "doc": "&quot;Remove the specified resource from storage.&quot;"},
            
            {"type": "Class", "fromName": "App\\Http\\Controllers", "fromLink": "App/Http/Controllers.html", "link": "App/Http/Controllers/DistributorController.html", "name": "App\\Http\\Controllers\\DistributorController", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Http\\Controllers\\DistributorController", "fromLink": "App/Http/Controllers/DistributorController.html", "link": "App/Http/Controllers/DistributorController.html#method_index", "name": "App\\Http\\Controllers\\DistributorController::index", "doc": "&quot;Display a listing of the resource.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\DistributorController", "fromLink": "App/Http/Controllers/DistributorController.html", "link": "App/Http/Controllers/DistributorController.html#method_create", "name": "App\\Http\\Controllers\\DistributorController::create", "doc": "&quot;Show the form for creating a new resource.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\DistributorController", "fromLink": "App/Http/Controllers/DistributorController.html", "link": "App/Http/Controllers/DistributorController.html#method_store", "name": "App\\Http\\Controllers\\DistributorController::store", "doc": "&quot;Store a newly created resource in storage.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\DistributorController", "fromLink": "App/Http/Controllers/DistributorController.html", "link": "App/Http/Controllers/DistributorController.html#method_show", "name": "App\\Http\\Controllers\\DistributorController::show", "doc": "&quot;Display the specified resource.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\DistributorController", "fromLink": "App/Http/Controllers/DistributorController.html", "link": "App/Http/Controllers/DistributorController.html#method_edit", "name": "App\\Http\\Controllers\\DistributorController::edit", "doc": "&quot;Show the form for editing the specified resource.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\DistributorController", "fromLink": "App/Http/Controllers/DistributorController.html", "link": "App/Http/Controllers/DistributorController.html#method_update", "name": "App\\Http\\Controllers\\DistributorController::update", "doc": "&quot;Update the specified resource in storage.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\DistributorController", "fromLink": "App/Http/Controllers/DistributorController.html", "link": "App/Http/Controllers/DistributorController.html#method_destroy", "name": "App\\Http\\Controllers\\DistributorController::destroy", "doc": "&quot;Remove the specified resource from storage.&quot;"},
            
            {"type": "Class", "fromName": "App\\Http\\Controllers", "fromLink": "App/Http/Controllers.html", "link": "App/Http/Controllers/GenreController.html", "name": "App\\Http\\Controllers\\GenreController", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Http\\Controllers\\GenreController", "fromLink": "App/Http/Controllers/GenreController.html", "link": "App/Http/Controllers/GenreController.html#method_indexView", "name": "App\\Http\\Controllers\\GenreController::indexView", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\GenreController", "fromLink": "App/Http/Controllers/GenreController.html", "link": "App/Http/Controllers/GenreController.html#method_index", "name": "App\\Http\\Controllers\\GenreController::index", "doc": "&quot;Display a listing of the resource.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\GenreController", "fromLink": "App/Http/Controllers/GenreController.html", "link": "App/Http/Controllers/GenreController.html#method_create", "name": "App\\Http\\Controllers\\GenreController::create", "doc": "&quot;Show the form for creating a new resource.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\GenreController", "fromLink": "App/Http/Controllers/GenreController.html", "link": "App/Http/Controllers/GenreController.html#method_store", "name": "App\\Http\\Controllers\\GenreController::store", "doc": "&quot;Store a newly created resource in storage.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\GenreController", "fromLink": "App/Http/Controllers/GenreController.html", "link": "App/Http/Controllers/GenreController.html#method_show", "name": "App\\Http\\Controllers\\GenreController::show", "doc": "&quot;Display the specified resource.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\GenreController", "fromLink": "App/Http/Controllers/GenreController.html", "link": "App/Http/Controllers/GenreController.html#method_edit", "name": "App\\Http\\Controllers\\GenreController::edit", "doc": "&quot;Show the form for editing the specified resource.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\GenreController", "fromLink": "App/Http/Controllers/GenreController.html", "link": "App/Http/Controllers/GenreController.html#method_update", "name": "App\\Http\\Controllers\\GenreController::update", "doc": "&quot;Update the specified resource in storage.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\GenreController", "fromLink": "App/Http/Controllers/GenreController.html", "link": "App/Http/Controllers/GenreController.html#method_destroy", "name": "App\\Http\\Controllers\\GenreController::destroy", "doc": "&quot;Remove the specified resource from storage.&quot;"},
            
            {"type": "Class", "fromName": "App\\Http\\Controllers", "fromLink": "App/Http/Controllers.html", "link": "App/Http/Controllers/HomeController.html", "name": "App\\Http\\Controllers\\HomeController", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Http\\Controllers\\HomeController", "fromLink": "App/Http/Controllers/HomeController.html", "link": "App/Http/Controllers/HomeController.html#method___construct", "name": "App\\Http\\Controllers\\HomeController::__construct", "doc": "&quot;Create a new controller instance.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\HomeController", "fromLink": "App/Http/Controllers/HomeController.html", "link": "App/Http/Controllers/HomeController.html#method_index", "name": "App\\Http\\Controllers\\HomeController::index", "doc": "&quot;Show the application dashboard.&quot;"},
            
            {"type": "Class", "fromName": "App\\Http\\Controllers", "fromLink": "App/Http/Controllers.html", "link": "App/Http/Controllers/MovieController.html", "name": "App\\Http\\Controllers\\MovieController", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Http\\Controllers\\MovieController", "fromLink": "App/Http/Controllers/MovieController.html", "link": "App/Http/Controllers/MovieController.html#method_index", "name": "App\\Http\\Controllers\\MovieController::index", "doc": "&quot;Display a listing of the resource.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\MovieController", "fromLink": "App/Http/Controllers/MovieController.html", "link": "App/Http/Controllers/MovieController.html#method_indexView", "name": "App\\Http\\Controllers\\MovieController::indexView", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\MovieController", "fromLink": "App/Http/Controllers/MovieController.html", "link": "App/Http/Controllers/MovieController.html#method_filter", "name": "App\\Http\\Controllers\\MovieController::filter", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\MovieController", "fromLink": "App/Http/Controllers/MovieController.html", "link": "App/Http/Controllers/MovieController.html#method_create", "name": "App\\Http\\Controllers\\MovieController::create", "doc": "&quot;Show the form for creating a new resource.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\MovieController", "fromLink": "App/Http/Controllers/MovieController.html", "link": "App/Http/Controllers/MovieController.html#method_store", "name": "App\\Http\\Controllers\\MovieController::store", "doc": "&quot;Store a newly created resource in storage.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\MovieController", "fromLink": "App/Http/Controllers/MovieController.html", "link": "App/Http/Controllers/MovieController.html#method_uploadImg", "name": "App\\Http\\Controllers\\MovieController::uploadImg", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\MovieController", "fromLink": "App/Http/Controllers/MovieController.html", "link": "App/Http/Controllers/MovieController.html#method_show", "name": "App\\Http\\Controllers\\MovieController::show", "doc": "&quot;Display the specified resource.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\MovieController", "fromLink": "App/Http/Controllers/MovieController.html", "link": "App/Http/Controllers/MovieController.html#method_edit", "name": "App\\Http\\Controllers\\MovieController::edit", "doc": "&quot;Show the form for editing the specified resource.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\MovieController", "fromLink": "App/Http/Controllers/MovieController.html", "link": "App/Http/Controllers/MovieController.html#method_update", "name": "App\\Http\\Controllers\\MovieController::update", "doc": "&quot;Update the specified resource in storage.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\MovieController", "fromLink": "App/Http/Controllers/MovieController.html", "link": "App/Http/Controllers/MovieController.html#method_destroy", "name": "App\\Http\\Controllers\\MovieController::destroy", "doc": "&quot;Remove the specified resource from storage.&quot;"},
            
            {"type": "Class", "fromName": "App\\Http\\Controllers", "fromLink": "App/Http/Controllers.html", "link": "App/Http/Controllers/MovieTimeController.html", "name": "App\\Http\\Controllers\\MovieTimeController", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Http\\Controllers\\MovieTimeController", "fromLink": "App/Http/Controllers/MovieTimeController.html", "link": "App/Http/Controllers/MovieTimeController.html#method_index", "name": "App\\Http\\Controllers\\MovieTimeController::index", "doc": "&quot;Display a listing of the resource.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\MovieTimeController", "fromLink": "App/Http/Controllers/MovieTimeController.html", "link": "App/Http/Controllers/MovieTimeController.html#method_filterByDate", "name": "App\\Http\\Controllers\\MovieTimeController::filterByDate", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\MovieTimeController", "fromLink": "App/Http/Controllers/MovieTimeController.html", "link": "App/Http/Controllers/MovieTimeController.html#method_indexView", "name": "App\\Http\\Controllers\\MovieTimeController::indexView", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\MovieTimeController", "fromLink": "App/Http/Controllers/MovieTimeController.html", "link": "App/Http/Controllers/MovieTimeController.html#method_create", "name": "App\\Http\\Controllers\\MovieTimeController::create", "doc": "&quot;Show the form for creating a new resource.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\MovieTimeController", "fromLink": "App/Http/Controllers/MovieTimeController.html", "link": "App/Http/Controllers/MovieTimeController.html#method_store", "name": "App\\Http\\Controllers\\MovieTimeController::store", "doc": "&quot;Store a newly created resource in storage.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\MovieTimeController", "fromLink": "App/Http/Controllers/MovieTimeController.html", "link": "App/Http/Controllers/MovieTimeController.html#method_show", "name": "App\\Http\\Controllers\\MovieTimeController::show", "doc": "&quot;Display the specified resource.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\MovieTimeController", "fromLink": "App/Http/Controllers/MovieTimeController.html", "link": "App/Http/Controllers/MovieTimeController.html#method_edit", "name": "App\\Http\\Controllers\\MovieTimeController::edit", "doc": "&quot;Show the form for editing the specified resource.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\MovieTimeController", "fromLink": "App/Http/Controllers/MovieTimeController.html", "link": "App/Http/Controllers/MovieTimeController.html#method_update", "name": "App\\Http\\Controllers\\MovieTimeController::update", "doc": "&quot;Update the specified resource in storage.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\MovieTimeController", "fromLink": "App/Http/Controllers/MovieTimeController.html", "link": "App/Http/Controllers/MovieTimeController.html#method_destroy", "name": "App\\Http\\Controllers\\MovieTimeController::destroy", "doc": "&quot;Remove the specified resource from storage.&quot;"},
            
            {"type": "Class", "fromName": "App\\Http\\Controllers", "fromLink": "App/Http/Controllers.html", "link": "App/Http/Controllers/RatingController.html", "name": "App\\Http\\Controllers\\RatingController", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Http\\Controllers\\RatingController", "fromLink": "App/Http/Controllers/RatingController.html", "link": "App/Http/Controllers/RatingController.html#method_index", "name": "App\\Http\\Controllers\\RatingController::index", "doc": "&quot;Display a listing of the resource.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\RatingController", "fromLink": "App/Http/Controllers/RatingController.html", "link": "App/Http/Controllers/RatingController.html#method_create", "name": "App\\Http\\Controllers\\RatingController::create", "doc": "&quot;Show the form for creating a new resource.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\RatingController", "fromLink": "App/Http/Controllers/RatingController.html", "link": "App/Http/Controllers/RatingController.html#method_store", "name": "App\\Http\\Controllers\\RatingController::store", "doc": "&quot;Store a newly created resource in storage.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\RatingController", "fromLink": "App/Http/Controllers/RatingController.html", "link": "App/Http/Controllers/RatingController.html#method_show", "name": "App\\Http\\Controllers\\RatingController::show", "doc": "&quot;Display the specified resource.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\RatingController", "fromLink": "App/Http/Controllers/RatingController.html", "link": "App/Http/Controllers/RatingController.html#method_edit", "name": "App\\Http\\Controllers\\RatingController::edit", "doc": "&quot;Show the form for editing the specified resource.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\RatingController", "fromLink": "App/Http/Controllers/RatingController.html", "link": "App/Http/Controllers/RatingController.html#method_update", "name": "App\\Http\\Controllers\\RatingController::update", "doc": "&quot;Update the specified resource in storage.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\RatingController", "fromLink": "App/Http/Controllers/RatingController.html", "link": "App/Http/Controllers/RatingController.html#method_destroy", "name": "App\\Http\\Controllers\\RatingController::destroy", "doc": "&quot;Remove the specified resource from storage.&quot;"},
            
            {"type": "Class", "fromName": "App\\Http\\Controllers", "fromLink": "App/Http/Controllers.html", "link": "App/Http/Controllers/ReportController.html", "name": "App\\Http\\Controllers\\ReportController", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Http\\Controllers\\ReportController", "fromLink": "App/Http/Controllers/ReportController.html", "link": "App/Http/Controllers/ReportController.html#method_index", "name": "App\\Http\\Controllers\\ReportController::index", "doc": "&quot;Display a listing of the resource.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\ReportController", "fromLink": "App/Http/Controllers/ReportController.html", "link": "App/Http/Controllers/ReportController.html#method_getReport", "name": "App\\Http\\Controllers\\ReportController::getReport", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\ReportController", "fromLink": "App/Http/Controllers/ReportController.html", "link": "App/Http/Controllers/ReportController.html#method_create", "name": "App\\Http\\Controllers\\ReportController::create", "doc": "&quot;Show the form for creating a new resource.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\ReportController", "fromLink": "App/Http/Controllers/ReportController.html", "link": "App/Http/Controllers/ReportController.html#method_store", "name": "App\\Http\\Controllers\\ReportController::store", "doc": "&quot;Store a newly created resource in storage.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\ReportController", "fromLink": "App/Http/Controllers/ReportController.html", "link": "App/Http/Controllers/ReportController.html#method_show", "name": "App\\Http\\Controllers\\ReportController::show", "doc": "&quot;Display the specified resource.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\ReportController", "fromLink": "App/Http/Controllers/ReportController.html", "link": "App/Http/Controllers/ReportController.html#method_edit", "name": "App\\Http\\Controllers\\ReportController::edit", "doc": "&quot;Show the form for editing the specified resource.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\ReportController", "fromLink": "App/Http/Controllers/ReportController.html", "link": "App/Http/Controllers/ReportController.html#method_update", "name": "App\\Http\\Controllers\\ReportController::update", "doc": "&quot;Update the specified resource in storage.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\ReportController", "fromLink": "App/Http/Controllers/ReportController.html", "link": "App/Http/Controllers/ReportController.html#method_destroy", "name": "App\\Http\\Controllers\\ReportController::destroy", "doc": "&quot;Remove the specified resource from storage.&quot;"},
            
            {"type": "Class", "fromName": "App\\Http\\Controllers", "fromLink": "App/Http/Controllers.html", "link": "App/Http/Controllers/RoleController.html", "name": "App\\Http\\Controllers\\RoleController", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Http\\Controllers\\RoleController", "fromLink": "App/Http/Controllers/RoleController.html", "link": "App/Http/Controllers/RoleController.html#method_index", "name": "App\\Http\\Controllers\\RoleController::index", "doc": "&quot;Display a listing of the resource.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\RoleController", "fromLink": "App/Http/Controllers/RoleController.html", "link": "App/Http/Controllers/RoleController.html#method_create", "name": "App\\Http\\Controllers\\RoleController::create", "doc": "&quot;Show the form for creating a new resource.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\RoleController", "fromLink": "App/Http/Controllers/RoleController.html", "link": "App/Http/Controllers/RoleController.html#method_store", "name": "App\\Http\\Controllers\\RoleController::store", "doc": "&quot;Store a newly created resource in storage.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\RoleController", "fromLink": "App/Http/Controllers/RoleController.html", "link": "App/Http/Controllers/RoleController.html#method_show", "name": "App\\Http\\Controllers\\RoleController::show", "doc": "&quot;Display the specified resource.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\RoleController", "fromLink": "App/Http/Controllers/RoleController.html", "link": "App/Http/Controllers/RoleController.html#method_edit", "name": "App\\Http\\Controllers\\RoleController::edit", "doc": "&quot;Show the form for editing the specified resource.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\RoleController", "fromLink": "App/Http/Controllers/RoleController.html", "link": "App/Http/Controllers/RoleController.html#method_update", "name": "App\\Http\\Controllers\\RoleController::update", "doc": "&quot;Update the specified resource in storage.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\RoleController", "fromLink": "App/Http/Controllers/RoleController.html", "link": "App/Http/Controllers/RoleController.html#method_destroy", "name": "App\\Http\\Controllers\\RoleController::destroy", "doc": "&quot;Remove the specified resource from storage.&quot;"},
            
            {"type": "Class", "fromName": "App\\Http\\Controllers", "fromLink": "App/Http/Controllers.html", "link": "App/Http/Controllers/RoomController.html", "name": "App\\Http\\Controllers\\RoomController", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Http\\Controllers\\RoomController", "fromLink": "App/Http/Controllers/RoomController.html", "link": "App/Http/Controllers/RoomController.html#method_index", "name": "App\\Http\\Controllers\\RoomController::index", "doc": "&quot;Display a listing of the resource.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\RoomController", "fromLink": "App/Http/Controllers/RoomController.html", "link": "App/Http/Controllers/RoomController.html#method_indexView", "name": "App\\Http\\Controllers\\RoomController::indexView", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\RoomController", "fromLink": "App/Http/Controllers/RoomController.html", "link": "App/Http/Controllers/RoomController.html#method_create", "name": "App\\Http\\Controllers\\RoomController::create", "doc": "&quot;Show the form for creating a new resource.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\RoomController", "fromLink": "App/Http/Controllers/RoomController.html", "link": "App/Http/Controllers/RoomController.html#method_store", "name": "App\\Http\\Controllers\\RoomController::store", "doc": "&quot;Store a newly created resource in storage.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\RoomController", "fromLink": "App/Http/Controllers/RoomController.html", "link": "App/Http/Controllers/RoomController.html#method_show", "name": "App\\Http\\Controllers\\RoomController::show", "doc": "&quot;Display the specified resource.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\RoomController", "fromLink": "App/Http/Controllers/RoomController.html", "link": "App/Http/Controllers/RoomController.html#method_edit", "name": "App\\Http\\Controllers\\RoomController::edit", "doc": "&quot;Show the form for editing the specified resource.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\RoomController", "fromLink": "App/Http/Controllers/RoomController.html", "link": "App/Http/Controllers/RoomController.html#method_update", "name": "App\\Http\\Controllers\\RoomController::update", "doc": "&quot;Update the specified resource in storage.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\RoomController", "fromLink": "App/Http/Controllers/RoomController.html", "link": "App/Http/Controllers/RoomController.html#method_destroy", "name": "App\\Http\\Controllers\\RoomController::destroy", "doc": "&quot;Remove the specified resource from storage.&quot;"},
            
            {"type": "Class", "fromName": "App\\Http\\Controllers", "fromLink": "App/Http/Controllers.html", "link": "App/Http/Controllers/TicketController.html", "name": "App\\Http\\Controllers\\TicketController", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Http\\Controllers\\TicketController", "fromLink": "App/Http/Controllers/TicketController.html", "link": "App/Http/Controllers/TicketController.html#method_index", "name": "App\\Http\\Controllers\\TicketController::index", "doc": "&quot;Display a listing of the resource.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\TicketController", "fromLink": "App/Http/Controllers/TicketController.html", "link": "App/Http/Controllers/TicketController.html#method_create", "name": "App\\Http\\Controllers\\TicketController::create", "doc": "&quot;Show the form for creating a new resource.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\TicketController", "fromLink": "App/Http/Controllers/TicketController.html", "link": "App/Http/Controllers/TicketController.html#method_store", "name": "App\\Http\\Controllers\\TicketController::store", "doc": "&quot;Store a newly created resource in storage.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\TicketController", "fromLink": "App/Http/Controllers/TicketController.html", "link": "App/Http/Controllers/TicketController.html#method_show", "name": "App\\Http\\Controllers\\TicketController::show", "doc": "&quot;Display the specified resource.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\TicketController", "fromLink": "App/Http/Controllers/TicketController.html", "link": "App/Http/Controllers/TicketController.html#method_edit", "name": "App\\Http\\Controllers\\TicketController::edit", "doc": "&quot;Show the form for editing the specified resource.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\TicketController", "fromLink": "App/Http/Controllers/TicketController.html", "link": "App/Http/Controllers/TicketController.html#method_update", "name": "App\\Http\\Controllers\\TicketController::update", "doc": "&quot;Update the specified resource in storage.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\TicketController", "fromLink": "App/Http/Controllers/TicketController.html", "link": "App/Http/Controllers/TicketController.html#method_destroy", "name": "App\\Http\\Controllers\\TicketController::destroy", "doc": "&quot;Remove the specified resource from storage.&quot;"},
            
            {"type": "Class", "fromName": "App\\Http", "fromLink": "App/Http.html", "link": "App/Http/Kernel.html", "name": "App\\Http\\Kernel", "doc": "&quot;&quot;"},
                    
            {"type": "Class", "fromName": "App\\Http\\Middleware", "fromLink": "App/Http/Middleware.html", "link": "App/Http/Middleware/Authenticate.html", "name": "App\\Http\\Middleware\\Authenticate", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Http\\Middleware\\Authenticate", "fromLink": "App/Http/Middleware/Authenticate.html", "link": "App/Http/Middleware/Authenticate.html#method_redirectTo", "name": "App\\Http\\Middleware\\Authenticate::redirectTo", "doc": "&quot;Get the path the user should be redirected to when they are not authenticated.&quot;"},
            
            {"type": "Class", "fromName": "App\\Http\\Middleware", "fromLink": "App/Http/Middleware.html", "link": "App/Http/Middleware/CheckForMaintenanceMode.html", "name": "App\\Http\\Middleware\\CheckForMaintenanceMode", "doc": "&quot;&quot;"},
                    
            {"type": "Class", "fromName": "App\\Http\\Middleware", "fromLink": "App/Http/Middleware.html", "link": "App/Http/Middleware/Cors.html", "name": "App\\Http\\Middleware\\Cors", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Http\\Middleware\\Cors", "fromLink": "App/Http/Middleware/Cors.html", "link": "App/Http/Middleware/Cors.html#method_handle", "name": "App\\Http\\Middleware\\Cors::handle", "doc": "&quot;Handle an incoming request.&quot;"},
            
            {"type": "Class", "fromName": "App\\Http\\Middleware", "fromLink": "App/Http/Middleware.html", "link": "App/Http/Middleware/EncryptCookies.html", "name": "App\\Http\\Middleware\\EncryptCookies", "doc": "&quot;&quot;"},
                    
            {"type": "Class", "fromName": "App\\Http\\Middleware", "fromLink": "App/Http/Middleware.html", "link": "App/Http/Middleware/RedirectIfAuthenticated.html", "name": "App\\Http\\Middleware\\RedirectIfAuthenticated", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Http\\Middleware\\RedirectIfAuthenticated", "fromLink": "App/Http/Middleware/RedirectIfAuthenticated.html", "link": "App/Http/Middleware/RedirectIfAuthenticated.html#method_handle", "name": "App\\Http\\Middleware\\RedirectIfAuthenticated::handle", "doc": "&quot;Handle an incoming request.&quot;"},
            
            {"type": "Class", "fromName": "App\\Http\\Middleware", "fromLink": "App/Http/Middleware.html", "link": "App/Http/Middleware/TrimStrings.html", "name": "App\\Http\\Middleware\\TrimStrings", "doc": "&quot;&quot;"},
                    
            {"type": "Class", "fromName": "App\\Http\\Middleware", "fromLink": "App/Http/Middleware.html", "link": "App/Http/Middleware/TrustProxies.html", "name": "App\\Http\\Middleware\\TrustProxies", "doc": "&quot;&quot;"},
                    
            {"type": "Class", "fromName": "App\\Http\\Middleware", "fromLink": "App/Http/Middleware.html", "link": "App/Http/Middleware/VerifyCsrfToken.html", "name": "App\\Http\\Middleware\\VerifyCsrfToken", "doc": "&quot;&quot;"},
                    
            {"type": "Class", "fromName": "App", "fromLink": "App.html", "link": "App/Movie.html", "name": "App\\Movie", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Movie", "fromLink": "App/Movie.html", "link": "App/Movie.html#method_getSearchResult", "name": "App\\Movie::getSearchResult", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Movie", "fromLink": "App/Movie.html", "link": "App/Movie.html#method_director", "name": "App\\Movie::director", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Movie", "fromLink": "App/Movie.html", "link": "App/Movie.html#method_distributor", "name": "App\\Movie::distributor", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Movie", "fromLink": "App/Movie.html", "link": "App/Movie.html#method_genre", "name": "App\\Movie::genre", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Movie", "fromLink": "App/Movie.html", "link": "App/Movie.html#method_rating", "name": "App\\Movie::rating", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Movie", "fromLink": "App/Movie.html", "link": "App/Movie.html#method_movieTimes", "name": "App\\Movie::movieTimes", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Movie", "fromLink": "App/Movie.html", "link": "App/Movie.html#method_report", "name": "App\\Movie::report", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "App", "fromLink": "App.html", "link": "App/MovieTime.html", "name": "App\\MovieTime", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\MovieTime", "fromLink": "App/MovieTime.html", "link": "App/MovieTime.html#method_movie", "name": "App\\MovieTime::movie", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\MovieTime", "fromLink": "App/MovieTime.html", "link": "App/MovieTime.html#method_room", "name": "App\\MovieTime::room", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\MovieTime", "fromLink": "App/MovieTime.html", "link": "App/MovieTime.html#method_displayType", "name": "App\\MovieTime::displayType", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\MovieTime", "fromLink": "App/MovieTime.html", "link": "App/MovieTime.html#method_tickets", "name": "App\\MovieTime::tickets", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "App\\Providers", "fromLink": "App/Providers.html", "link": "App/Providers/AppServiceProvider.html", "name": "App\\Providers\\AppServiceProvider", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Providers\\AppServiceProvider", "fromLink": "App/Providers/AppServiceProvider.html", "link": "App/Providers/AppServiceProvider.html#method_register", "name": "App\\Providers\\AppServiceProvider::register", "doc": "&quot;Register any application services.&quot;"},
                    {"type": "Method", "fromName": "App\\Providers\\AppServiceProvider", "fromLink": "App/Providers/AppServiceProvider.html", "link": "App/Providers/AppServiceProvider.html#method_boot", "name": "App\\Providers\\AppServiceProvider::boot", "doc": "&quot;Bootstrap any application services.&quot;"},
            
            {"type": "Class", "fromName": "App\\Providers", "fromLink": "App/Providers.html", "link": "App/Providers/AuthServiceProvider.html", "name": "App\\Providers\\AuthServiceProvider", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Providers\\AuthServiceProvider", "fromLink": "App/Providers/AuthServiceProvider.html", "link": "App/Providers/AuthServiceProvider.html#method_boot", "name": "App\\Providers\\AuthServiceProvider::boot", "doc": "&quot;Register any authentication \/ authorization services.&quot;"},
            
            {"type": "Class", "fromName": "App\\Providers", "fromLink": "App/Providers.html", "link": "App/Providers/BroadcastServiceProvider.html", "name": "App\\Providers\\BroadcastServiceProvider", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Providers\\BroadcastServiceProvider", "fromLink": "App/Providers/BroadcastServiceProvider.html", "link": "App/Providers/BroadcastServiceProvider.html#method_boot", "name": "App\\Providers\\BroadcastServiceProvider::boot", "doc": "&quot;Bootstrap any application services.&quot;"},
            
            {"type": "Class", "fromName": "App\\Providers", "fromLink": "App/Providers.html", "link": "App/Providers/EventServiceProvider.html", "name": "App\\Providers\\EventServiceProvider", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Providers\\EventServiceProvider", "fromLink": "App/Providers/EventServiceProvider.html", "link": "App/Providers/EventServiceProvider.html#method_boot", "name": "App\\Providers\\EventServiceProvider::boot", "doc": "&quot;Register any events for your application.&quot;"},
            
            {"type": "Class", "fromName": "App\\Providers", "fromLink": "App/Providers.html", "link": "App/Providers/RouteServiceProvider.html", "name": "App\\Providers\\RouteServiceProvider", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Providers\\RouteServiceProvider", "fromLink": "App/Providers/RouteServiceProvider.html", "link": "App/Providers/RouteServiceProvider.html#method_boot", "name": "App\\Providers\\RouteServiceProvider::boot", "doc": "&quot;Define your route model bindings, pattern filters, etc.&quot;"},
                    {"type": "Method", "fromName": "App\\Providers\\RouteServiceProvider", "fromLink": "App/Providers/RouteServiceProvider.html", "link": "App/Providers/RouteServiceProvider.html#method_map", "name": "App\\Providers\\RouteServiceProvider::map", "doc": "&quot;Define the routes for the application.&quot;"},
                    {"type": "Method", "fromName": "App\\Providers\\RouteServiceProvider", "fromLink": "App/Providers/RouteServiceProvider.html", "link": "App/Providers/RouteServiceProvider.html#method_mapWebRoutes", "name": "App\\Providers\\RouteServiceProvider::mapWebRoutes", "doc": "&quot;Define the \&quot;web\&quot; routes for the application.&quot;"},
                    {"type": "Method", "fromName": "App\\Providers\\RouteServiceProvider", "fromLink": "App/Providers/RouteServiceProvider.html", "link": "App/Providers/RouteServiceProvider.html#method_mapApiRoutes", "name": "App\\Providers\\RouteServiceProvider::mapApiRoutes", "doc": "&quot;Define the \&quot;api\&quot; routes for the application.&quot;"},
            
            {"type": "Class", "fromName": "App", "fromLink": "App.html", "link": "App/Rating.html", "name": "App\\Rating", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Rating", "fromLink": "App/Rating.html", "link": "App/Rating.html#method_movies", "name": "App\\Rating::movies", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Rating", "fromLink": "App/Rating.html", "link": "App/Rating.html#method_getSearchResult", "name": "App\\Rating::getSearchResult", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "App", "fromLink": "App.html", "link": "App/Report.html", "name": "App\\Report", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Report", "fromLink": "App/Report.html", "link": "App/Report.html#method_movie", "name": "App\\Report::movie", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "App", "fromLink": "App.html", "link": "App/Role.html", "name": "App\\Role", "doc": "&quot;&quot;"},
                    
            {"type": "Class", "fromName": "App", "fromLink": "App.html", "link": "App/Room.html", "name": "App\\Room", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Room", "fromLink": "App/Room.html", "link": "App/Room.html#method_getSearchResult", "name": "App\\Room::getSearchResult", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Room", "fromLink": "App/Room.html", "link": "App/Room.html#method_movieTime", "name": "App\\Room::movieTime", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "App", "fromLink": "App.html", "link": "App/Ticket.html", "name": "App\\Ticket", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Ticket", "fromLink": "App/Ticket.html", "link": "App/Ticket.html#method_movieTime", "name": "App\\Ticket::movieTime", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "App", "fromLink": "App.html", "link": "App/User.html", "name": "App\\User", "doc": "&quot;&quot;"},
                    
            
                                        // Fix trailing commas in the index
        {}
    ];

    /** Tokenizes strings by namespaces and functions */
    function tokenizer(term) {
        if (!term) {
            return [];
        }

        var tokens = [term];
        var meth = term.indexOf('::');

        // Split tokens into methods if "::" is found.
        if (meth > -1) {
            tokens.push(term.substr(meth + 2));
            term = term.substr(0, meth - 2);
        }

        // Split by namespace or fake namespace.
        if (term.indexOf('\\') > -1) {
            tokens = tokens.concat(term.split('\\'));
        } else if (term.indexOf('_') > 0) {
            tokens = tokens.concat(term.split('_'));
        }

        // Merge in splitting the string by case and return
        tokens = tokens.concat(term.match(/(([A-Z]?[^A-Z]*)|([a-z]?[^a-z]*))/g).slice(0,-1));

        return tokens;
    };

    root.Sami = {
        /**
         * Cleans the provided term. If no term is provided, then one is
         * grabbed from the query string "search" parameter.
         */
        cleanSearchTerm: function(term) {
            // Grab from the query string
            if (typeof term === 'undefined') {
                var name = 'search';
                var regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
                var results = regex.exec(location.search);
                if (results === null) {
                    return null;
                }
                term = decodeURIComponent(results[1].replace(/\+/g, " "));
            }

            return term.replace(/<(?:.|\n)*?>/gm, '');
        },

        /** Searches through the index for a given term */
        search: function(term) {
            // Create a new search index if needed
            if (!bhIndex) {
                bhIndex = new Bloodhound({
                    limit: 500,
                    local: searchIndex,
                    datumTokenizer: function (d) {
                        return tokenizer(d.name);
                    },
                    queryTokenizer: Bloodhound.tokenizers.whitespace
                });
                bhIndex.initialize();
            }

            results = [];
            bhIndex.get(term, function(matches) {
                results = matches;
            });

            if (!rootPath) {
                return results;
            }

            // Fix the element links based on the current page depth.
            return $.map(results, function(ele) {
                if (ele.link.indexOf('..') > -1) {
                    return ele;
                }
                ele.link = rootPath + ele.link;
                if (ele.fromLink) {
                    ele.fromLink = rootPath + ele.fromLink;
                }
                return ele;
            });
        },

        /** Get a search class for a specific type */
        getSearchClass: function(type) {
            return searchTypeClasses[type] || searchTypeClasses['_'];
        },

        /** Add the left-nav tree to the site */
        injectApiTree: function(ele) {
            ele.html(treeHtml);
        }
    };

    $(function() {
        // Modify the HTML to work correctly based on the current depth
        rootPath = $('body').attr('data-root-path');
        treeHtml = treeHtml.replace(/href="/g, 'href="' + rootPath);
        Sami.injectApiTree($('#api-tree'));
    });

    return root.Sami;
})(window);

$(function() {

    // Enable the version switcher
    $('#version-switcher').change(function() {
        window.location = $(this).val()
    });

    
        // Toggle left-nav divs on click
        $('#api-tree .hd span').click(function() {
            $(this).parent().parent().toggleClass('opened');
        });

        // Expand the parent namespaces of the current page.
        var expected = $('body').attr('data-name');

        if (expected) {
            // Open the currently selected node and its parents.
            var container = $('#api-tree');
            var node = $('#api-tree li[data-name="' + expected + '"]');
            // Node might not be found when simulating namespaces
            if (node.length > 0) {
                node.addClass('active').addClass('opened');
                node.parents('li').addClass('opened');
                var scrollPos = node.offset().top - container.offset().top + container.scrollTop();
                // Position the item nearer to the top of the screen.
                scrollPos -= 200;
                container.scrollTop(scrollPos);
            }
        }

    
    
        var form = $('#search-form .typeahead');
        form.typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        }, {
            name: 'search',
            displayKey: 'name',
            source: function (q, cb) {
                cb(Sami.search(q));
            }
        });

        // The selection is direct-linked when the user selects a suggestion.
        form.on('typeahead:selected', function(e, suggestion) {
            window.location = suggestion.link;
        });

        // The form is submitted when the user hits enter.
        form.keypress(function (e) {
            if (e.which == 13) {
                $('#search-form').submit();
                return true;
            }
        });

    
});


