@extends('layouts.layout')
@section('title','Películas')

@section('content')
  
 <!-- HEADER -->
 <div class="columns">
     
        <div class="column">
            <nav class="breadcrumb" aria-label="breadcrumbs">
                <ul>
                    <li >
                        <a href="/movies">
                            Películas
                        </a>
                    </li>
                    <li class="is-active">
                        
                        <a href="/movies">
                            {{ substr($genre, strpos($genre,',') + 1) }}
                        </a>
                    </li>    
                </ul>
            </nav>
        </div>
        <div class="column">
            <button class="button is-link is-pulled-right" onclick="location.href='/movies/new'">Nueva</button>
        </div>
    </div>
<!-- END HEADER -->
        



<div class="columns">
    <div class="column is-four-fifths">
        <nav class="categories scroll" style="width:100%"> 
           
                <div v-for="genre in genres" style="margin-bottom: 1em">
                            <span @click="filterByGenre(genre.id, genre.genre)"> 
                                    !{ genre.genre }!
                            </span>                                      
                    </div>
            
        </nav>
    </div>
    <div class="column">
                <input type="text"  placeholder="buscar..." class="input">
       </div>
</div>

    <div class="container scroll"  style="width: 980px; height: 375px">
        <!--Compare if the array is empty -->

        <!--printing the movies -->  

              
        <!-- CARD -->
        <div class="movies">                  
            @foreach($movieByGenre as $movie)
                        <div class="card" style="width: 150px">
                            <header class="card-header">
                                <img src="/{{$movie->poster}}" alt="">
                            </header>
                            <div class="card-content" >
                                        <div class="content" style="white-space: pre-line;">
                                                        <b>{{$movie->title }}</b>                                                                        
                                                        {{$movie->year }}   
                                                                 
                                                        {{$movie->genre->genre }}            
                                        </div>
                            </div>
                        </div>  
                   
                        @endforeach
            </div>
      
        </div>
        <!-- CARD -->
        </div>
        <!-- CARD -->
        
            
         

   
@endsection
