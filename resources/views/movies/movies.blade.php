@extends('layouts.layout')
@section('title','Películas')

@section('content')
  
 <!-- HEADER -->
 <div class="columns">
     
        <div class="column">
            <nav class="breadcrumb" aria-label="breadcrumbs">
                <ul>
                    <li class="is-active">
                        <a href="#">Películas</a>
                    </li>    
                </ul>
            </nav>
        </div>
        <div class="column">
            <button class="button is-link is-pulled-right" onclick="location.href='/movies/new'">Nueva</button>
        </div>
    </div>
<!-- END HEADER -->
        



<div class="columns">
    <div class="column is-four-fifths">
        <nav class="categories scroll" style="width:100%"> 
           
                <div v-for="genre in genres" style="margin-bottom: 1em">
                    <span @click="filterByGenre(genre.id, genre.genre)">
                        !{ genre.genre }!
                       
                    </span>  
                    </div>
            
        </nav>
    </div>
    <div class="column">
                <input type="text"  placeholder="buscar..." class="input">
       </div>
</div>

    <div class="container scroll"  style="width: 980px; height: 375px">
        <!--Compare if the array is empty -->

        <!--printing the movies -->  

              
        <!-- CARD -->
        <div v-for="movies in movieArray">
            <div class="movies">
                    <div v-for="movie in movies">
                        <div class="card" style="width: 150px">
                           
                            <header class="card-header">
                                <img  v-bind:src="movie.poster" alt="">
                            </header>
                            <div class="card-content">            
                                        <div class="content name" style="white-space: pre-line;">
                                         <a  v-bind:href="'/movies/' + movie.id">
                                                        <b>!{ movie.title }!</b>
                                        </a>
                                                        !{ movie.year }!
                                                        
                                                        
                                                        !{ movie.genre.genre }!                  
                                        </div>
                            </div>
                            
                        </div>
                    </div>
            </div>
        </div>
        <!-- CARD -->
        </div>

   
@endsection
