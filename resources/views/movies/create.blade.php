@extends('layouts.layout')
@section('title','Películas')

@section('content')
  
 <!-- HEADER -->
 <div class="columns">
     
        <div class="column">
            <nav class="breadcrumb" aria-label="breadcrumbs">
                <ul>
                    <li>
                        <a href="/movies">Películas</a>
                        <li class="is-active"><a href="#" aria-current="page">Nueva</a></li>
                    </li>    
                </ul>
            </nav>
        </div>
        <div class="column">
            <button class="button is-warning is-pulled-right" onclick="location.href='/movies'">Regresar</button>
        </div>
    </div>
<!-- END HEADER -->
<div class="section formulario scroll">

        @csrf
        <div class="field">
            <label for="" class="label">Título</label>
            <p class="control has-icons-left">
                <input class="input" type="text" v-model="title" placeholder="Título">
                <span class="icon is-small is-left">
                    <i class="fas fa-lock"></i>
                </span>
            </p>
        </div>
        <!-- Poster -->
        <label for="" class="label">Póster</label>
            <form action="{{ URL::to('/upload') }}" method="post" enctype="multipart/form-data">
                @csrf
                    <div class="field" >
                        <div class="file has-name is-fullwidth">
                                <label class="file-label">
                                    <input class="file-input" type="file" @change="getImage" name="file">
                                <span class="file-cta">
                                <span class="file-icon">
                                    <i class="fas fa-upload"></i>
                                </span>
                                <span class="file-label" >
                                    Subir imagen...
                                </span>
                                </span>
                                <input class="file-name" v-model="poster" readonly>
                                    
                                
                            </label>
                        </div>
                    </div>
                 <button class="button" type="submit">Subir</button>
            
            </form>
       
        
       
        <!-- end-Poster -->
        
        <!-- Synopsis -->
        <div class="field">
            <label for="" class="label">Sinópsis</label>
            <p class="control has-icons-left">
                <textarea class="textarea" type="text" v-model="synopsis" placeholder="Sinópsis"></textarea>
                <span class="icon is-small is-left">
                    <i class="fas fa-lock"></i>
                </span>
            </p>
        </div>
        <!-- end-Synopsis -->


        <!-- Year -->
        <div class="field">
            <label for="" class="label">Año</label>
            <p class="control has-icons-left">
                <input class="input" type="text" v-model="year" placeholder="Año">
                <span class="icon is-small is-left">
                    <i class="fas fa-lock"></i>
                </span>
            </p>
        </div>
        <!-- end-Year -->


        <!-- Duration -->
        <div class="field">
            <label for="" class="label">Duración</label>
            <p class="control has-icons-left">
                <input class="input" type="text" v-model="duration" placeholder="Duración">
                <span class="icon is-small is-left">
                    <i class="fas fa-lock"></i>
                </span>
            </p>
        </div>
        <!-- end-Duration -->

        <!-- Director -->
        <div class="columns" style="margin-bottom:-20px ">
         <div class="column">
                 <p class="control">
                 <label for="" class="label">Director</label>
                    <input type="text" class="input" v-model="director" readonly>
                 </p>
            </div>
            <div class="column">
           
                    <div class="field">
                        <label for="" class="label">Buscar</label>
                        <p class="control has-icons-left">
                            <input class="input" v-model="query"  type="text" placeholder="Buscar director...">
                            <input type="hidden" v-model="directorId">
                            <ul class="list is-hoverable" v-if="directorsSearch.length > 0 && query">
                                <li v-for="result in directorsSearch.slice(0,10)" :key="result.id">
                                    <a :href="result.url" class="list-item">
                                        <div v-text="result.fullname" v-on:click="displayDirector(result.fullname, result.id)"></div>
                                        
                                    </a>
                                </li>
                            </ul>
                            <span class="icon is-small is-left">
                                <i class="fas fa-lock"></i>
                            </span>
                        </p>
                    </div>
            </div>
           
        </div>
       
        <!-- end-Director -->

        <!-- Distri -->
        <div class="columns" style="margin-bottom:-20px ">
             <div class="column">
                 <p class="control">
                 <label for="" class="label">Distribuidor</label>
                    <input type="text" class="input" v-model="distributor" readonly>
                 </p>
            </div>
            <div class="column">         
                    <div class="field">
                        <label for="" class="label">Buscar</label>
                        <p class="control has-icons-left">
                            <input class="input" v-model="queryD"  type="text" placeholder="Buscar distribuidor...">
                            <input type="hidden" v-model="distributorId">
                                <ul class="list is-hoverable" v-if="distributorsSearch.length > 0 && queryD">
                                    <li v-for="result in distributorsSearch.slice(0,10)" :key="result.id">
                                        <a :href="result.url" class="list-item">
                                            <div v-text="result.name" v-on:click="displayDistributor(result.name, result.id)"></div>
                                            
                                        </a>
                                    </li>
                                </ul>
                            <span class="icon is-small is-left">
                                <i class="fas fa-lock"></i>
                            </span>
                        </p>
                    </div>
            </div>
           
        </div>
        <!-- end-Distr -->

        <!-- Genre -->
        <div class="columns" style="margin-bottom:-20px ">
             <div class="column">
                 <p class="control">
                 <label for="" class="label">Género</label>
                    <input type="text" class="input" v-model="genre" readonly>
                 </p>
            </div>
            <div class="column">         
                    <div class="field">
                        <label for="" class="label">Buscar</label>
                        <p class="control has-icons-left">
                            <input class="input" v-model="queryG"  type="text" placeholder="Buscar género...">
                            <input type="hidden" v-model="genreId">
                                 <ul class="list is-hoverable" v-if="genresSearch.length > 0 && queryG">
                                    <li v-for="result in genresSearch.slice(0,10)" :key="result.id">
                                        <a :href="result.url" class="list-item">
                                            <div v-text="result.genre" v-on:click="displayGenre(result.genre, result.id)"></div>
                                            
                                        </a>
                                    </li>
                                </ul>
                            <span class="icon is-small is-left">
                                <i class="fas fa-lock"></i>
                            </span>
                        </p>
                    </div>
            </div>
           
        </div>
        <!-- end-Genre -->


        <!-- Rating -->
        <div class="columns" style="margin-bottom:-20px ">
             <div class="column">
                 <p class="control">
                 <label for="" class="label">Clasificación</label>
                    <input type="text" class="input" v-model="rating" readonly>
                 </p>
            </div>
            <div class="column">         
                    <div class="field">
                        <label for="" class="label">Buscar</label>
                        <p class="control has-icons-left">
                            <input class="input" v-model="queryR"  type="text" placeholder="Buscar clasificación...">
                            <input type="hidden" v-model="ratingId">
                                    <ul class="list is-hoverable" v-if="ratingsSearch.length > 0 && queryR">
                                        <li v-for="result in ratingsSearch.slice(0,10)" :key="result.id">
                                            <a :href="result.url" class="list-item">
                                                <div v-text="result.rating" v-on:click="displayRating(result.rating, result.id)"></div>
                                                
                                            </a>
                                        </li>
                                    </ul>
                            <span class="icon is-small is-left">
                                <i class="fas fa-lock"></i>
                            </span>
                        </p>
                    </div>
            </div>
        </div>
        <!-- end-Rating -->
    <button class="button is-info" @click="addMovie">Guardar</button>
   
    </div>
</div>
@endsection
