@extends('layouts.layout')
@section('title','Películas')

@section('content')
  
 <!-- HEADER -->
 <div class="columns">
     
        <div class="column">
            <nav class="breadcrumb" aria-label="breadcrumbs">
                <ul>
                    <li>
                        <a href="/movies">Películas</a>
                    </li> 
                    <li class="is-active">
                        <a href="">{{ $movie->title }}</a>
                    </li>    
                </ul>
            </nav>
        </div>
        <div class="column">
            <button class="button is-link is-pulled-right" onclick="location.href='/movies/new'">Nueva</button>
        </div>
    </div>
<!-- END HEADER -->

<div class="section scroll" style="width: 980px; height: 420px">
    <div class="columns">
        <div class="column">
            <div class="card" style="width: 250px">           
                        <img  src="/{{$movie->poster}}" alt="">                            
            </div>
        </div>
        <div class="column">
            <p></p>
            <p><h2 class="title">{{ $movie->title }}</h2></p>
            <p><b>año:</b> {{ $movie->year }}</p>
            <p><b>sinópsis:</b> {{ $movie->synopsis }}</p>
            <p><b>duración:</b> {{ $movie->duration }}</p>
            <p><b>género:</b> {{ $movie->genre->genre }}</p>
            <p><b>clasificación:</b> {{ $movie->rating->rating }}</p>
            <p><b>director:</b> {{ $movie->director->fullname }}</p>
            <p><b>distribuidor:</b> {{ $movie->distributor->name }}</p>
            @if($having)
            <form action="/reports" method="post">
             @csrf
             @method('PATCH')
             <input type="hidden" name="id" value="{{$movie->id}}">
            <button class="button" type="submit" style="background: #41B9A8; color: white;">
                Ver reporte
            </button>
            </form>
            @endif
            
        </div>
    </div>
</div>

@endsection
