@extends('layouts.layout')
@section('title','Películas')

@section('content')
  
 <!-- HEADER -->
 <div class="columns">
     
        <div class="column">
            <nav class="breadcrumb" aria-label="breadcrumbs">
                <ul>
                    <li>
                        <a href="/reports">Reportes</a>
                    </li> 
                    <li class="is-active">
                        <a href="">{{ $report->movie->title }}</a>
                    </li>    
                </ul>
            </nav>
        </div>
        <div class="column">
            <button class="button is-warning is-pulled-right" onclick="location.href='/reports'">Regresar</button>
        </div>
    </div>
<!-- END HEADER -->

<div class="section scroll" style="width: 980px; height: 420px">
    <div class="columns">
        <div class="column">
            <div class="card" style="width: 250px">           
                        <img  src="/{{$report->movie->poster}}" alt="">                            
            </div>
        </div>
        <div class="column">
            <p></p>
            <p><h2 class="title">{{ $report->movie->title }}</h2></p>
             <hr>
             <p><b>Taquilla: </b> Q {{ $report->box_office }}</p>
            <p><b>2d: </b>{{ $report->tickets_2d }}</p>
             <p><b>3d: </b>{{ $report->tickets_3d }}</p>
            <p><b>Total de boletos:  </b>{{ $report->ticket_amount }}</p>
            <hr>
            <p><b>Iva a pagar:</b> {{ $report->iva_tax }}</p>
            <p><b>Derechos de Autor:</b> {{$report->copyright }}</p>
            <p><b>Utilidades del distribuidor:</b> {{ $report->distributor_utilities }}</p>
            <p><b>Utilidades del exhibidor:</b> {{ $report->film_exhibitor_utilities }}</p>
             <hr>
        </div>
    </div>
</div>

@endsection
