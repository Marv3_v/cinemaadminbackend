@extends('layouts.layout')
@section('title','Reportes')

@section('content')
  
 <!-- HEADER -->
 <div class="columns">
     
        <div class="column">
            <nav class="breadcrumb" aria-label="breadcrumbs">
                <ul>
                    <li class="is-active">
                        <a href="#">Reportes</a>
                    </li>    
                </ul>
            </nav>
        </div>
        <div class="column">
            
        </div>
    </div>
<!-- END HEADER -->
         <div class="container scroll"  style="width: 980px; height: 460px">
        <!--Compare if the array is empty -->

        <!--printing the reports -->  
        <p>Precio de tickets en 3d: Q40</p>
        <p>Precio de tickets en 2d: Q30</p>
              
        <!-- CARD -->
        <div class="movies">
        @foreach($reports as $report)
                        <div class="card" style="width: 260px">
                            
                            <div class="card-content" >
                                        <div class="content" style="white-space: pre-line;">
                                                          <b>{{ $report->movie->title }}</b>
                                                          <p><b>Taquilla: </b> Q {{ $report->box_office }}</p>
                                                          <hr>
                                                          <a href="/reports/{{$report->id}}" class="button is-warning">Detalle</a>
                                                          
                                        </div>
                            </div>
                            <header class="card-header">
                            </header>
                        </div> 
        @endforeach
        </div>
       
        <!-- CARD -->
        </div>
@endsection
