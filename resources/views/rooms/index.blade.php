@extends('layouts.layout')
@section('title','Películas')

@section('content')
  
 <!-- HEADER -->
 <div class="columns">
     
        <div class="column">
            <nav class="breadcrumb" aria-label="breadcrumbs">
                <ul>
                    <li class="is-active">
                        <a href="#">Salas</a>
                    </li>    
                </ul>
            </nav>
        </div>
        <div class="column">
            <button class="button is-link is-pulled-right" onclick="location.href='/rooms/new'">Nueva</button>
        </div>
    </div>
<!-- END HEADER -->
  <div class="container scroll"  style="width: 980px; height: 360px">
        <!--Compare if the array is empty -->

        <!--printing the rooms -->  

              
        <!-- CARD -->
        <div class="movies">
        @foreach($rooms as $room)
                        <div class="card" style="width: 180px">
                            
                            <div class="card-content" >
                                        <div class="content" style="white-space: pre-line;">
                                                          <p><b>Nombre: </b>{{ $room->number }}</p>
                                                          <p><b>Capacidad: </b>{{ $room->chairs_amount }}</p>
                                        </div>
                            </div>
                            <header class="card-header">
                            </header>
                        </div> 
        @endforeach
        </div>
        <!-- CARD -->
        </div>
   
@endsection
