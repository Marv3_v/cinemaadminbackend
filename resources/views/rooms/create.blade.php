@extends('layouts.layout')
@section('title','Salas')

@section('content')
  
 <!-- HEADER -->
 <div class="columns">
     
        <div class="column">
            <nav class="breadcrumb" aria-label="breadcrumbs">
                <ul>
                    <li>
                        <a href="/rooms">Salas</a>
                        <li class="is-active"><a href="#" aria-current="page">Nueva</a></li>
                    </li>    
                </ul>
            </nav>
        </div>
        <div class="column">
            <button class="button is-warning is-pulled-right" onclick="location.href='/rooms'">Regresar</button>
        </div>
    </div>
<!-- END HEADER -->
<div class="section formulario scroll">
<form action="/rooms" method="post">
    @csrf
     <!-- number -->
        <div class="field">
            <label for="" class="label">Número de sala</label>
            <p class="control has-icons-left">
                <input class="input" type="text" name="number" placeholder="número...">
                <span class="icon is-small is-left">
                    <i class="fas fa-lock"></i>
                </span>
            </p>
        </div>
    <!-- number -->     

     <!-- chairs_amount -->
        <div class="field">
            <label for="" class="label">Capacidad</label>
            <p class="control has-icons-left">
                <input class="input" type="number" name="chairs_amount" placeholder="capacidad...">
                <span class="icon is-small is-left">
                    <i class="fas fa-lock"></i>
                </span>
            </p>
        </div>
    <!-- chairs_amount -->       
    <button class="button is-info" type="submit">Guardar</button>
</form>
</div>
@endsection
