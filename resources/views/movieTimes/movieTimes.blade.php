@extends('layouts.layout')
@section('title','Películas')

@section('content')
  
 <!-- HEADER -->
 <div class="columns">
     
        <div class="column">
            <nav class="breadcrumb" aria-label="breadcrumbs">
                <ul>
                    <li class="is-active">
                        <a href="#">Funciones</a>
                    </li>    
                </ul>
            </nav>
        </div>
        <div class="column">
            <button class="button is-link is-pulled-right" onclick="location.href='/movieTimes/new'">Nueva</button>
        </div>
    </div>
<!-- END HEADER -->
    <div class="columns">
        <div class="column is-four-fifths">
        <div class="control">
            <form action="" method="get" id="dateForm">
                <label for="" class="label">Buscar por fecha</label>      
                <span><input type="date" v-model="dateFilter" class="input" @change="searchByDate"></span>  
            </form>
        </div>
                
        </div>
        <div class="column">
            <div class="control">
                <label for="" class="label">Taquilla</label>
                    <a href="/tickets" class="button">Venta de Tickets</a>
            </div>
        </div>
    </div>


     <div class="container scroll"  style="width: 980px; height: 360px">
        <!--Compare if the array is empty -->

        <!--printing the movies -->  

              
        <!-- CARD -->
        <div class="movies">
        <div v-for="movieT in movieTimeArray">
                        <div class="card" style="width: 200px">
                            
                            <div class="card-content" >
                                        <div class="content" style="white-space: pre-line;">
                                                          <b>!{ movieT.movie.title }!</b>
                                                          !{ movieT.movie.genre.genre }!
                                                          <hr>
                                                        <b>Fecha: </b> !{ movieT.movie_time_date }!
                                                          <b>!{ movieT.beginning_schedule }!</b>
                                                          !{ movieT.room.number }!
                                                          !{ movieT.display_type.type }!
                                                          <hr>
                                                          <p><b>Boleto:  </b>Q!{ movieT.price }!</p>
                                                          <p><b>Descuento:  </b> !{ movieT.discount }!%</p>
                                                          <p><b>Total: </b>Q!{ movieT.total }!</p>
                                                          <hr>
                                                          <div v-if="movieT.has_tickets == 1">
                                                            Ya hay una o varias tickets vendidas
                                                          </div>
                                        </div>
                            </div>
                            <header class="card-header">
                            </header>
                        </div> 
            </div>
        </div>
        <!-- CARD -->
        </div>

   
@endsection
