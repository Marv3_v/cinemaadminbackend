@extends('layouts.layout')
@section('title','Películas')

@section('content')
  
 <!-- HEADER -->
 <div class="columns">
     
        <div class="column">
            <nav class="breadcrumb" aria-label="breadcrumbs">
                <ul>
                    <li>
                        <a href="/movieTimes">Funciones</a>
                        <li class="is-active"><a href="#" aria-current="page">Nueva</a></li>
                    </li>    
                </ul>
            </nav>
        </div>
        <div class="column">
            <button class="button is-warning is-pulled-right" onclick="location.href='/movieTimes'">Regresar</button>
        </div>
    </div>
<!-- END HEADER -->
<div class="section formulario scroll">
    <!-- movie -->
        <div class="columns" style="margin-bottom:-20px ">
         <div class="column">
                 <p class="control">
                 <label for="" class="label">Película</label>
                    <input type="text" class="input" v-model="movie" readonly>
                 </p>
            </div>
            <div class="column">
           
                    <div class="field">
                        <label for="" class="label">Buscar</label>
                        <p class="control has-icons-left">
                            <input class="input" v-model="queryM"  type="text" placeholder="Buscar película...">
                            <input type="hidden" v-model="movieId">
                                <ul class="list is-hoverable" v-if="moviesSearch.length > 0 && queryM">
                                    <li v-for="result in moviesSearch.slice(0,10)" :key="result.id">
                                        <a :href="result.url" class="list-item">
                                            <div v-text="result.title" v-on:click="displayMovie(result.title, result.id)"></div>
                                            
                                        </a>
                                    </li>
                                </ul>
                            <span class="icon is-small is-left">
                                <i class="fas fa-lock"></i>
                            </span>
                        </p>
                    </div>
            </div>
           
        </div>
    <!-- end-movie -->  
     <!-- date -->
        <div class="field">
            <label for="" class="label">Fecha</label>
            <p class="control has-icons-left">
                <input class="input" type="date" v-model="movieTimeDate" placeholder="Fecha...">
                <span class="icon is-small is-left">
                    <i class="fas fa-lock"></i>
                </span>
            </p>
        </div>
    <!-- end-date -->     

     <!-- beginning_schedule -->
        <div class="field">
            <label for="" class="label">Horario</label>
            <p class="control has-icons-left">
                <input class="input" type="text" v-model="schedule" placeholder="horario...">
                <span class="icon is-small is-left">
                    <i class="fas fa-lock"></i>
                </span>
            </p>
        </div>
    <!-- end-beginning_schedule -->       
    <!-- room -->
        <div class="columns" style="margin-bottom:-20px ">
         <div class="column">
                 <p class="control">
                 <label for="" class="label">Sala</label>
                    <input type="text" class="input" v-model="room" readonly>
                 </p>
            </div>
            <div class="column">
           
                    <div class="field">
                        <label for="" class="label">Buscar</label>
                        <p class="control has-icons-left">
                            <input class="input" v-model="queryRoom"  type="text" placeholder="Buscar sala...">
                            <input type="hidden" v-model="roomId">
                                <ul class="list is-hoverable" v-if="roomsSearch.length > 0 && queryRoom">
                                    <li v-for="result in roomsSearch.slice(0,10)" :key="result.id">
                                        <a :href="result.url" class="list-item">
                                            <div v-text="result.number" v-on:click="displayRoom(result.number, result.id)"></div>
                                            
                                        </a>
                                    </li>
                                </ul>
                            <span class="icon is-small is-left">
                                <i class="fas fa-lock"></i>
                            </span>
                        </p>
                    </div>
            </div>
           
        </div>
    <!-- end-room --> 
        <!-- displayType -->
        <div class="columns" style="margin-bottom:-20px ">
         <div class="column">
                 <p class="control">
                 <label for="" class="label">Tipo</label>
                    <input type="text" class="input" v-model="type" v-onchange="selectCost()" readonly>
                 </p>
            </div>
            <div class="column">
           
                    <div class="field">
                        <label for="" class="label">Buscar</label>
                        <p class="control has-icons-left">
                            <input class="input" v-model="queryDT"  type="text" placeholder="Buscar tipo...">
                            <input type="hidden" v-model="typeId">
                                <ul class="list is-hoverable" v-if="typesSearch.length > 0 && queryDT">
                                    <li v-for="result in typesSearch.slice(0,10)" :key="result.id">
                                        <a :href="result.url" class="list-item">
                                            <div v-text="result.type" v-on:click="displayType(result.type, result.id)"></div>
                                            
                                        </a>
                                    </li>
                                </ul>
                            <span class="icon is-small is-left">
                                <i class="fas fa-lock"></i>
                            </span>
                        </p>
                    </div>
            </div>
           
        </div>
    <!-- end-displayType -->
    <!-- ticket -->
        <div class="field">
            <label for="" class="label">Precio boleto</label>
            <p class="control has-icons-left">
                <input readonly class="input" type="text" v-model="ticket" placeholder="precio...">
                <span class="icon is-small is-left">
                    <i class="fas fa-lock"></i>
                </span>
            </p>
        </div>
    <!-- ticket -->     
     <!-- discount -->
        <div class="field">
            <label for="" class="label">Descuento en el boleto %</label>
            <p class="control has-icons-left">
                <input class="input" type="number" v-model="discount" v-onchange="getTotal()" placeholder="Descuento...">
                <span class="icon is-small is-left">
                    <i class="fas fa-lock"></i>
                </span>
            </p>
        </div>
    <!-- discount -->  
     <!-- total -->
        <div class="field">
            <label for="" class="label">Total</label>
            <p class="control has-icons-left">
                <input readonly class="input" type="number" v-model="total" placeholder="Total...">
                <span class="icon is-small is-left">
                    <i class="fas fa-lock"></i>
                </span>
            </p>
        </div>
    <!-- total -->  
    <button class="button is-info" @click="addMovieTime">Guardar</button>
</div>
@endsection
