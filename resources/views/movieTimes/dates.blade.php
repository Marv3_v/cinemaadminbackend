@extends('layouts.layout')
@section('title','Películas')

@section('content')
  
 <!-- HEADER -->
 <div class="columns">
     
        <div class="column">
            <nav class="breadcrumb" aria-label="breadcrumbs">
                <ul>
                    <li>
                        <a href="/movieTimes">Funciones</a>
                    </li>
                    <li class="is-active">
                        <a href="#">{{ $date }}</a>
                    </li>      
                </ul>
            </nav>
        </div>
        <div class="column">
            <button class="button is-link is-pulled-right" onclick="location.href='/movieTimes/new'">Nueva</button>
        </div>
    </div>
<!-- END HEADER -->
    <div class="columns">
        <div class="column is-four-fifths">
        <div class="control">
            <form action="" method="get" id="dateForm">
                <label for="" class="label">Buscar por fecha</label>      
                <span><input type="date" v-model="dateFilter" class="input" @change="searchByDate"></span>  
            </form>
        </div>
                
        </div>
        <div class="column">
            <div class="control">
                <label for="" class="label">Buscar por nombre</label>
                    <input type="text"  placeholder="buscar..." class="input">
            </div>
        </div>
    </div>


     <div class="container scroll"  style="width: 980px; height: 360px">
        <!--Compare if the array is empty -->

        <!--printing the movies -->  

              
        <!-- CARD -->
        <div class="movies">
        @foreach($movieTimes as $movieT)
                        <div class="card" style="width: 180px">
                            
                            <div class="card-content" >
                                        <div class="content" style="white-space: pre-line;">
                                                          <b>{{ $movieT->movie->title }}</b>
                                                          {{ $movieT->movie->genre->genre }}
                                                          <hr>
                                                          <b>Fecha: </b>{{ $movieT->movie_time_date }}
                                                            {{ $movieT->beginning_schedule }}
                                                            {{ $movieT->room->number }}
                                                            {{ $movieT->displayType->type }}
                                        </div>
                            </div>
                            <header class="card-header">
                            </header>
                        </div> 
          @endforeach
        </div>
        <!-- CARD -->
        </div>

   
@endsection
