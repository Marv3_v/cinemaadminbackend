<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Funciones</title>
    <link rel="stylesheet" href="{{ asset('css/style.css')}}">
    <link rel="stylesheet" href="{{ asset('css/bulma.css')}}">
    <link rel="stylesheet" href="{{ asset('css/navbar.css')}}">
    <link rel="stylesheet" href="{{ asset('css/sidebar.css')}}">
    <link rel="stylesheet" href="{{ asset('css/styles.css')}}">
</head>
<body>

<div class="container">
    <hr>
    <div id="app">
         
        <div class="level">
                <nav class="breadcrumb" aria-label="breadcrumbs">
                <ul>
                    <li class="is-active">
                        <a href="#">Funciones</a>
                    </li>    
                </ul>
            </nav>
            <div class="level-left">
            </div>
            <div class="level-right">
                <div class="field">
                <div class="control has-icons-left">
                    <input type="text" class="input is-info" placeholder="Buscar"  />
                    <span class="icon is-small is-left">
                    <i class="fas fa-search"></i>
                    </span>
                </div>
                </div>
            </div>
        </div>
       



<div class="container scroll"  style="width: 1300px; height: 460px">
                <div class="movies">
        <!--CICLO -->
        @foreach($movieTimes as $movieT)
                
               
                    <div class="card" style="width: 300px">
                            
                            <div class="card-content" >
                                        <div class="content" style="white-space: pre-line;">
                                                          <b>{{ $movieT->movie->title }}</b>
                                                          {{ $movieT->movie->genre->genre }}
                                                          <hr>
                                                        <b>Fecha: </b> {{ $movieT->movie_time_date }}   <b>{{ $movieT->beginning_schedule }}</b>
                                                          {{ $movieT->room->number }}
                                                          {{ $movieT->displayType->type }}
                                                          <hr>
                                                          <p><b>Boleto:  </b>Q{{ $movieT->price }}</p>
                                                          <p><b>Descuento:  </b> {{ $movieT->discount }}%</p>
                                                          <p><b>Total: </b>Q{{ $movieT->total }}</p>
                                                          <b>Tickets restantes: </b><?php echo $movieT->room->chairs_amount - $movieT->tickets->count(); ?> 
                                                          <input type="hidden" id="ticketPrice" value="{{$movieT->price}}">
                                                          <input type="hidden" id="ticketDiscount" value="{{$movieT->discount}}">
                                                          <input type="hidden" id="ticketTotal" value="{{$movieT->total}}">
                                                          <input type="hidden" id="tMovieTimeId" value="{{$movieT->id}}">
                                                          <hr>
                                                          
                                                          
                                                          <input type="number" class="input" @keyup="whatNumber()" id="ticketNumber" style="width:100px">
                                                          @if( ($movieT->room->chairs_amount - $movieT->tickets->count())===0 ) 
                                                                <button @click="sellTicket()" disabled class="button is-link">Vender boleto/s</button>
                                                          @else 
                                                                 <button @click="sellTicket()" class="button is-link">Vender boleto/s</button>
                                                          @endif
                                                          <hr>
                                                          @if($movieT->has_tickets==1)
                                                            Ya hay una o varias tickets vendidas
                                                          @else
                                                            Ninguna ticket vendida
                                                          @endif
                                                          
                                        </div>
                            </div>
                        </div> 
                        @endforeach
        <!--CICLO -->
                 </div>
       </div>
        
    </div>
    <hr>
        <a href="/movieTimes">Admin</a>
    <hr />
</div>
</body>
<script src="{{ asset('js/vue.js') }}"></script>
<script src="{{ asset('js/axios.min.js') }}"></script>
<script>
const app = new Vue({
    el: '#app',
    delimiters: ['!{', '}!'],
    data: {
        movieTimeArray: "",
    },
    mounted() {
        
    },
    methods: {
            sellTicket() {
                ticketsNumber = document.getElementById('ticketNumber').value;
                    console.log(ticketsNumber);
                if(ticketsNumber <=5 ) {
                    
                    for(var i=1;i<=ticketsNumber;i++) {
                            //console.log("Tickets: " + ticketsNumber);
                         
                            var today = new Date();
                            var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
                            var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
                            var dateTime = date+' '+time;

                           //selling some tickets with API
                                window.axios.post("/api/tickets", {
                                    price: document.getElementById('ticketPrice').value,
                                    discount: document.getElementById('ticketDiscount').value,
                                    total: document.getElementById('ticketTotal').value,
                                    employee_name: 'Robert',
                                    print_time: dateTime,
                                    movie_time_id: document.getElementById('tMovieTimeId').value,
                                })
                                
                    }
                    
                     window.location.href = "/tickets";
                               
                }
                
            },
            whatNumber() {
                if(document.getElementById('ticketNumber').value >= 6)  {
                    
                    document.getElementById('ticketNumber').value = "5";

                } else if(document.getElementById('ticketNumber').value <=0){
                          document.getElementById('ticketNumber').value = "1";
                }  
            }
               
    }
});
</script>
</html>