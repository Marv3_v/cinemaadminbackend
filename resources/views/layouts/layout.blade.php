<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="{{ asset('css/style.css')}}">
    <link rel="stylesheet" href="{{ asset('css/bulma.css')}}">
    <link rel="stylesheet" href="{{ asset('css/navbar.css')}}">
    <link rel="stylesheet" href="{{ asset('css/sidebar.css')}}">
</head>
<body>
<div id="app">
        <div class="columns">
                <!-- SIDEBAR STARTS -->
                        <aside class="column is-2 is-hidden-mobile">
                    <div class="panel">
                        <div id="cde">
                            <p class="panel-heading has-text-centered"><b>Admin</b></p>
                        </div>
                        <a class="panel-block" href="/" id="home">                            
                                    <span class="panel-icon">
                                        <i class="fa fa-home"></i>
                                    </span>
                                    <span class="name">Inicio</span>
                        </a>
                        <a class="panel-block" href="/movies" id="movie">
                            <span class="panel-icon">
                                <i class="fa fa-table"></i>
                            </span>
                            <span class="name">Películas</span>
                        </a>
                        <a class="panel-block" href="/rooms">
                            <span class="panel-icon">
                                <i class="fa fa-table"></i>
                            </span>
                            <span class="name">Salas</span>
                        </a>
                        <a class="panel-block" href="/movieTimes">
                            <span class="panel-icon">
                                <i class="fa fa-th-list"></i>
                            </span>
                            <span class="name">Funciones</span>
                        </a>
                        <a class="panel-block" href="/reports">
                            <span class="panel-icon">
                                <i class="fa fa-list"></i>
                            </span>
                            <span class="name">Reporte</span>
                        </a>
                        <a class="panel-block">
                            <span class="panel-icon">
                                <i class="fa fa-list"></i>
                            </span>
                            <span class="name">Ajustes</span>
                        </a>
                        
                    </div>
                </aside>
                <!-- SIDEBAR ENDS -->
                <div class="column" style="padding: 0%">
                        
                    <!-- NAVBAR STARTS -->
                    <div>
                        <nav class="navbar">
                        <div class="navbar-brand">
                        
                        <div class="navbar-burger burger" data-target="navMenu">
                        <span></span>
                        <span></span>
                        <span></span>
                        </div>
                        </div>
                        </nav>
                        </div>
                    <!-- NAVBAR STARTS -->
                    
                    <!-- CONTENT STARTS WITH A COLUMN-->
                
                    <section class="column" id="cine">
                            
                        <!-- Place all the content you want to display here -->
                            @yield('content')
                       
                        <!-- CONTENT ENDS -->
                        
                    </section>
                </div>
            </div>
            <!-- COLUMNS END -->
    </div>
</body>
<script src="{{ asset('js/vue.js') }}"></script>
<script src="{{ asset('js/axios.min.js') }}"></script>

<script>
    
    const app = new Vue({
        el: '#cine',
        delimiters: ['!{', '}!'],
        data: {
            msg: "hola",
            genres: [],
            movieArray: '',
            //Director
            query: null,
            directorsSearch: [],
            director: "",
            directorId: "",
            //Distributor
            queryD: null,
            distributorsSearch: [],
            distributor: "",
            distributorId: "",
            //Genre
            queryG: null,
            genresSearch: [],
            genre: "",
            genreId: "",
            //Rating
            queryR: null,
            ratingsSearch: [],
            rating: "",
            ratingId: "",
            //Movie
            queryM: null,
            moviesSearch: [],
            movie: "",
            movieId: "",
            //Room
            queryRoom: null,
            roomsSearch: [],
            room: "",
            roomId: "",
            //DisplayType
            queryDT: null,
            typesSearch: [],
            type: "",
            typeId: "",
            //inserting a movie
            title: "",
            synopsis: "",
            poster: "",
            year: "",
            duration: "",
            directorId: "",
            distributorId: "",
            genreId: "",
            ratingId: "",
            //inserting a movieTime
            movieMT: "",
            movieTimeDate: "",
            schedule: "",
            roomMT: "",
            typeMT: "",
            ticket: "",
            discount: "0",
            total: "",
            //getting the image from input to print it then
            imgSelected: "",
            //active class
            activeItem: "home",
            //movieTimes
            movieTimeArray: '',
            //get MovieTimes per Date
            dateFilter: '',
            //get a movie
            oneMovie: "",
            oneMovieId: "",
            movieGenre: {},
            oneM: ""
        },
        watch: {
           query(after, before) {
                this.searchDirectors();
           },
           queryD(after, before) {
                 this.searchDistributors();
           },
           queryG(after, before) {
               this.searchGenres();
           },
           queryR(after, before) {
                this.searchRatings();
           },
           queryM(after, before) {
                this.searchMovies();
           },
           queryRoom(after, before) {
               this.searchRooms();
           },
           queryDT(after, before) {
               this.searchTypes();
           },
             
        },
        mounted() {
            this.loadGenres();
            this.loadMovies();
            this.loadMovieTimes();
        },
        
        methods: {
            getCurrentDate() {
                
            },   
            loadGenres(){
                window.axios.get("/api/genres").then(({ data }) => {
                    this.genres = data;
                    
                });
            },
           displayDirector(name, id) {
                this.director = name;
                this.directorId = id; 
                this.query = null;
           },
           displayDistributor(name, id) {
                this.distributor = name;
                this.distributorId = id; 
                this.queryD = null;
           },
           displayGenre(name, id) {
                this.genre = name;
                this.genreId = id; 
                this.queryG = null;
           },
           displayRating(name, id) {
                this.rating = name;
                this.ratingId = id; 
                this.queryR = null;
           },
           displayMovie(name, id) {
                this.movie = name;
                this.movieId = id; 
                this.queryM = null;
           },
           displayRoom(name, id) {
               this.room = name;
               this.roomId = id;
               this.queryRoom = null;
           },
           displayType(name, id) {
               this.type = name;
               this.typeId = id;
               this.queryDT = null;
           },
           //getting movies from API
            loadMovies() {
                window.axios.get("/api/movies").then(({ data }) => {
                    this.movieArray = data;
                    
                });
            },
            //getting id from a movie
            
            filterByGenre(id, name) {
                    console.log(id);
                    genre = [id, name];
                    console.log(genre);
                    window.location.href = "/movies/genre/" + genre;
            },
             //getting movieTimes from API
            loadMovieTimes() {
                window.axios.get("/api/movieTimes").then(({ data }) => {
                    this.movieTimeArray = data;
                });
            },
             //getting movieTimes from API
            searchByDate(){
                 if(this.dateFilter!= null){
                    document.getElementById('dateForm').action = "/movieTimes/"+ this.dateFilter;
                    document.getElementById('dateForm').submit();
                   
                 }
                 return true;
            },
            //inserting a movie with API
            addMovie() {
                console.log("antes de");
                window.axios.post("/api/movies", {
                    title: this.title,
                    synopsis: this.synopsis,
                    poster: "imgs/" + this.poster,
                    year: this.year,
                    duration: this.duration,
                    director_id: this.directorId,
                    distributor_id: this.distributorId,
                    genre_id: this.genreId,
                    rating_id: this.ratingId
                })
                .then(response => {
                    window.location.href = "/movies";
                    console.log("despues de");
                });
            },
            //getting the image (poster)
            getImage(event) {
                this.poster = event.target.files[0].name;
                console.log(event);
                this.imgSelected = event.target.files[0];
            },
            //inserting a movie with API
            addMovieTime() {
                console.log("antes de");
                window.axios.post("/api/movieTimes", {
                    movie_id: this.movieId,
                    movie_time_date: this.movieTimeDate,
                    beginning_schedule: this.schedule,
                    price: this.ticket,
                    discount: this.discount,
                    total: this.total,
                    room_id: this.roomId,
                    display_type_id: this.typeId,
                })
                .then(response => {
                    window.location.href = "/movieTimes";
                    console.log("despues de");
                });
            },
            searchDirectors() {
                window.axios.get("/api/directors", { params: { query: this.query } }).then(response => {
                  this.directorsSearch = response.data;
             })
            .catch(error => {}); 
            },
            searchDistributors() {
                window.axios.get("/api/distributors", { params: { queryD: this.queryD } }).then(response => {
                  this.distributorsSearch = response.data;
             })
            .catch(error => {}); 
            },
            searchGenres() {
                window.axios.get("/api/genres", { params: { queryG: this.queryG } }).then(response => {
                  this.genresSearch = response.data;
             })
            .catch(error => {}); 
            },
            searchRatings() {
                window.axios.get("/api/ratings", { params: { queryR: this.queryR } }).then(response => {
                  this.ratingsSearch = response.data;
             })
            .catch(error => {}); 
            },
            searchMovies() {
                window.axios.get("/api/movies", { params: { queryM: this.queryM } }).then(response => {
                  this.moviesSearch = response.data;
             })
            .catch(error => {}); 
            },
            searchRooms() {
                window.axios.get("/api/rooms", { params: { queryRoom: this.queryRoom } }).then(response => {
                  this.roomsSearch = response.data;
             })
            .catch(error => {}); 
            },
            searchTypes() {
                window.axios.get("/api/displayTypes", { params: { queryDT: this.queryDT } }).then(response => {
                  this.typesSearch = response.data;
             })
            .catch(error => {}); 
            },
            selectCost(){
                if(this.type != "") {
                    if(this.type == "2D") {
                            this.ticket = "30";
                            this.total = "30";
                            console.log('30');
                    } 
                    if(this.type == "3D") {
                            this.ticket = "40";
                            this.total = "40";
                            console.log('40');
                    }
                }
            },
            getTotal() {
                if(this.ticket!="") {
                    this.ticket = this.total;
                   
                    if(this.discount!="") {
                        less = (this.ticket/100) * this.discount;
                        this.total = this.ticket - less;
                    } else if(this.discount=="")  {
                         this.ticket = this.total;
                        console.log(this.ticket);
                        console.log(this.total);
                    }
                }
            },
           
        }

            });
        
    
</script>
</html>