@extends('layouts.layout')
@section('title','Inicio')
@section('content')
<nav class="breadcrumb" aria-label="breadcrumbs">
    <ul>
        <li class="is-active">
            <a href="#">Inicio</a>
        </li>    
    </ul>
</nav>
                    <!-- Place all the content you want to display here -->
                        <div class="tile is-ancestor">
                            <div class="tile is-vertical is-12">
                                <div class="tile">
                                    <div class="tile is-parent is-vertical">
                                        <article class="tile is-child notification is-primary">
                                            <p class="title">{{ $totalTickets }}</p>
                                            <p class="subtitle">Boletos vendidos</p>
                                        </article>                   
                                    </div>
                                    <div class="tile is-parent is-vertical">
                                        <article class="tile is-child notification is-success">
                                            <p class="title">Q{{ $totalMoney }}</p>
                                            <p class="subtitle">Facturación</p>
                                        </article>
                                    </div>
                                    <div class="tile is-parent is-vertical">
                                        <article class="tile is-child notification is-primary">
                                            <p class="title"> Q{{ $exhibitor }}</p>
                                            <p class="subtitle">Utilidades</p>
                                        </article>
                                    </div>
                                    
                            </div>
                           
                        </div>
@endsection