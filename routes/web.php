<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

//Route::get('/movies', 'MovieController@index');
Route::get('/movies', 'GenreController@indexView');
Route::get('/rooms', 'RoomController@indexView');
Route::get('/reports', 'ReportController@index');
Route::get('/rooms/new', 'RoomController@create');
Route::get('/movies/new', 'MovieController@create');
Route::get('/movies/{id}', 'MovieController@show');
Route::get('/reports/{id}', 'ReportController@show');
Route::get('/movies/genre/{genre}', 'MovieController@indexView');
Route::get('/movieTimes', 'MovieTimeController@indexView');
Route::get('/movieTimes/new', 'MovieTimeController@create');
Route::get('/movieTimes/{date}', 'MovieTimeController@filterByDate');
Route::post('/upload', 'MovieController@uploadImg');
Route::post('/rooms', 'RoomController@store');
Route::get('/tickets', 'TicketController@index');
Route::post('/reports', 'ReportController@store');
Route::patch('/reports', 'MovieController@update');
Route::get('/', 'ReportController@getReport');
Route::get('/home', 'HomeController@index')->name('home');
