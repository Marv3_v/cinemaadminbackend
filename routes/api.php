<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::resource('directors','DirectorController');
Route::resource('genres','GenreController');
Route::resource('ratings','RatingController');
Route::resource('distributors','DistributorController');
Route::resource('rooms','RoomController');
Route::resource('movies','MovieController');
Route::resource('movieTimes','MovieTimeController');
Route::resource('displayTypes','DisplayTypeController');
Route::resource('tickets','TicketController');
Route::resource('reports','ReportController');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});




