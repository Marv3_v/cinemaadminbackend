<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MovieTime extends Model
{
    protected $fillable = [
        'movie_time_date',
        'beginning_schedule',
        'price',
        'discount',
        'total',
        'is_active',
        'has_tickets',
        'movie_id',
        'room_id',
        'display_type_id',
    ];
    
    public function movie() {
        return $this->belongsTo('App\Movie');
    }

    public function room() {
        return $this->belongsTo('App\Room');
    }
    
    public function displayType() {
        return $this->belongsTo('App\DisplayType');
    }
    public function tickets() {
        return $this->hasMany('App\Ticket');
    }

}
