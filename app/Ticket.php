<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $fillable = [
        'price',
        'discount',
        'total',
        'movie_time_id',
        'employee_name',
        'print_time'
    ];

    public function movieTime() {
        return $this->belongsTo('App\MovieTime');
    }
}
