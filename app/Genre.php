<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;

class Genre extends Model implements Searchable
{
    protected $fillable = ['genre','deleted'];

    public function movies() {
        return $this->hasMany('App\Movie');
    }

     public function getSearchResult(): SearchResult
    {
        $url = route('genres.show', $this->id);
        return new SearchResult($this, $this->genre, $url);
    }
}
