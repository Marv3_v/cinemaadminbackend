<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;

class Rating extends Model implements Searchable
{
    protected $fillable = ['rating','deleted'];

    public function movies() {
        return $this->hasMany('App\Movie');
    }

    public function getSearchResult(): SearchResult {
        $url = route('ratings.show', $this->id);
        return new SearchResult($this, $this->rating, $url);
    }
}
