<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    protected $fillable = [
        'tickets_amount',
        'tickets_2d',
        'tickets_3d',
        'box_office',
        'distributor_utilities',
        'film_exhibitor_utilities'
    ];

    public function movie() {
        return $this->belongsTo('App\Movie');
    }
}
