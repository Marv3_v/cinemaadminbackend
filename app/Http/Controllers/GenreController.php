<?php

namespace App\Http\Controllers;

use App\Genre;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Spatie\Searchable\Search;

class GenreController extends Controller
{
   
    public function indexView() {
        return view('movies.movies');
    }

 /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request) {

        if($request->has('queryG')) {
                  $results = (new Search())
                ->registerModel(Genre::class, 'genre')
                ->search($request->input('queryG'))
                ->pluck('searchable');
        } else {
           
            return response()->json(Genre::all()->where('deleted',0));

        }

        return response()->json($results);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateGenre = $request->validate([
                 'genre' =>
                     array(
                        'required',
                        'regex:/^[a-zA-Z]+$/u',
                         'max:80'
                     )
            ]);
        $genre = Genre::create($validateGenre);
        return response()->json($genre, 202);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Genre  $genre
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $count = Genre::where('id', $id)->count();
        
        if($count ==1){
            $genre = Genre::find($id);
                return response()->json($genre);
        } else {
            return response()->json(
                
                ['msg' => 'Género no encontrado'],
                404
            );
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Genre  $genre
     * @return \Illuminate\Http\Response
     */
    public function edit(Genre $genre)
    {
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Genre  $genre
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $id)
    {
        $count = Genre::where('id', $id)->count();

        if($count == 1) {
            Genre::find($id)->update(
                ['genre'=>$request->updated]
            );
            return response()->json(
                ['msg' => 'yes'], 200
            );
        } else {
            return response()-json(
                ['msg' => 'No hay nada'], 404
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Genre  $genre
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $genre = Genre::where('id', $id)->first()->get();
        if (is_null($genre)) {
            return response()->json(['msg'=>'falló'], 404);
        }else{
            
            Genre::find($id)->update(['deleted'=>1]);
            return response()->json([
                'msg'=>'Actualizado'
            ]);
        }
    }
}