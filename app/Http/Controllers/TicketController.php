<?php

namespace App\Http\Controllers;

use App\Ticket;
use App\MovieTime;
use App\Movie;
use Illuminate\Http\Request;

class TicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

           $movieTimes = MovieTime::orderBy('movie_time_date')->where('is_active', 1)->get();
            foreach($movieTimes as $movieTime) {
                $movies[] = $movieTime->movie;
                $movieTime->room;
                $movieTime->tickets;
                $movieTime->displayType;
                foreach($movies as $movie) {
                        $movie->genre;
                        $movie->rating;
                    }
                } 
        return view('tickets.index', compact('movieTimes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $movieTime = MovieTime::find($id);
        $movieTime->movie;
        $movieTime->movie->genre;
        

        
        return view('tickets.create', compact('movieTime'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ticket = Ticket::create($request->all());
        
        $movieTime = MovieTime::find($request->movie_time_id);
        $movieTime->update(['has_tickets'=>1]);
        $movie = Movie::find($movieTime->movie_id);
        $movie->update(['has_tickets'=>1]);
        return response()->json($ticket);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function show(Ticket $ticket)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function edit(Ticket $ticket)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ticket $ticket)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ticket $ticket)
    {
        //
    }
}
