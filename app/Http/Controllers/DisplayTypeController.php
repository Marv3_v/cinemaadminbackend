<?php

namespace App\Http\Controllers;

use App\DisplayType;
use Illuminate\Http\Request;
use Spatie\Searchable\Search;

class DisplayTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->has('queryDT')) { 
            
            $results = (new Search())
                ->registerModel(DisplayType::class, 'type')
                ->search($request->input('queryDT'))
                ->pluck('searchable');
        } else {
            $results = DisplayType::all();
        }

        return response()->json(
            $results
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DisplayType  $displayType
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $count = DisplayType::where('id', $id)->count();
        
        if($count ==1){
            $type = DisplayType::find($id);
                return response()->json($type);
        } else {
            return response()->json(
                
                ['msg' => 'Tipo no encontrado'],
                404
            );
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DisplayType  $displayType
     * @return \Illuminate\Http\Response
     */
    public function edit(DisplayType $displayType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DisplayType  $displayType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DisplayType $displayType)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DisplayType  $displayType
     * @return \Illuminate\Http\Response
     */
    public function destroy(DisplayType $displayType)
    {
        //
    }
}
