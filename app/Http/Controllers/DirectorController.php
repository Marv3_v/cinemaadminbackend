<?php

namespace App\Http\Controllers;

use App\Director;
use Illuminate\Http\Request;
use Spatie\Searchable\Search;


class DirectorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->has('query')) { 
            
            $results = (new Search())
                ->registerModel(Director::class, 'fullname')
                ->search($request->input('query'))
                ->pluck('searchable');
        } else {
         return response()->json(
            Director::all()->where('deleted',0)
        );
    
        }
        return response()->json($results);
       }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateDirector = $request->validate([
                 'fullname' =>
                     array(
                        'required',
                        'regex:/^[a-zA-Z]+$/u',
                         'max:80'
                     )
            ]);
        $director = Director::create($validateDirector);
        return response()->json($director, 202);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Director  $director
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $count = Director::where('id', $id)->count();
        
        if($count ==1){
            $director = Director::find($id);
                return response()->json($director);
        } else {
            return response()->json(
                
                ['msg' => 'Director no encontrado'],
                404
            );
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Director  $director
     * @return \Illuminate\Http\Response
     */
    public function edit(Director $director)
    {
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Director  $director
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $id)
    {
        $count = Director::where('id', $id)->count();

        if($count == 1) {
            Director::find($id)->update(
                ['fullname'=>$request->updated]
            );
            return response()->json(
                ['msg' => 'yes'], 200
            );
        } else {
            return response()-json(
                ['msg' => 'No hay nada'], 404
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Director  $director
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $director = Director::where('id', $id)->first()->get();
        if (is_null($director)) {
            return response()->json(['msg'=>'falló'], 404);
        }else{
            
            Director::find($id)->update(['deleted'=>1]);
            return response()->json([
                'msg'=>'Actualizado'
            ]);
        }
    }
}
