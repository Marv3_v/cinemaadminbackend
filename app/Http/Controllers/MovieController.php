<?php

namespace App\Http\Controllers;

use App\Movie;
use App\Genre;
use App\MovieTime;
use App\Report;
use Symfony\Component\Console\Input\Input;
use Illuminate\Http\Request;
use Spatie\Searchable\Search;

class MovieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
         if($request->has('queryM')) { 
            
            $results = (new Search())
                ->registerModel(Movie::class, 'title')
                ->search($request->input('queryM'))
                ->pluck('searchable');
                return response()->json($results->where('active',1));

        } else {
            
            $results = Movie::all()->where('active',1);
            foreach($results as $movie) {
                    $movieGenre = $movie->genre;
            }     
            return response()->json(array($results));
        }
      
    }

    public function indexView($genre) {

        $movieByGenre = Movie::all()->where('genre_id', $genre)->where('active',1);
        
        return view('movies.genre', compact('movieByGenre', 'genre'));
    }

    public function filter($genre) {
         $movieByGenre = Movie::all()->where('genre_id', $genre)->where('active',1);
                return response()->json(array(
                    $movieByGenre
                ));         
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('movies/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $movie = Movie::create($request->all());
        //return view('movies.movies');
        return response()->json($movie, 201);
    }

    public function uploadImg(Request $request) {   
       
        $file = $request->file('file');
       $request->file('file')->move( base_path(). '/public/imgs/', $file->getClientOriginalName());

       echo '<img style="width: 500px" src="imgs/' .  $file->getClientOriginalName()  .'" />';
       
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $count = Movie::where('id', $id)->count();
            $movie = Movie::find($id);
            $movie->genre;
            $rating = $movie->rating;
            $director = $movie->director;
            $distributor = $movie->distributor;
            $movieTimes = $movie->movieTimes;
            $hasTicketsNumber = 0;
            $noTicketsNumber = 0;

            $hasTicketsNumber = $movieTimes->where('has_tickets',1)->count();
            $noTicketsNumber = $movieTimes->where('has_tickets',0)->count();

            if($movie->has_movie_times==0 && $movie->has_tickets==0 && $hasTicketsNumber < $noTicketsNumber){
                //No tiene funciones
                $having = false;
            } else  if($movie->has_movie_times==1 && $movie->has_tickets==0 && $hasTicketsNumber < $noTicketsNumber){
                $having = false;
            } else if($movie->has_movie_times==1 && $movie->has_tickets==1 && $hasTicketsNumber > $noTicketsNumber) {
                $having = true;
            } else {
                $having = false;
            }
            //return response()->json($movie);
            return view('movies.show', compact('movie', 'having'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function edit(Movie $movie)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $count = Movie::where('id', $request->id)->count();
        $movieM = Movie::find($request->id);
        if($count == 1) {
            
            
             $movieTimes = MovieTime::where('is_active', 1)->get();
            //if movietimes is not empty
            if($movieTimes->count()>=1) {
                $movieTimesById = MovieTime::where('movie_id', $request->id)->get();
                
                if($movieTimesById->count()>=1) {  

                    //Updates the movie and movietime too
                    $movieM->update(['active'=>0]);
                    $movieM->update(['has_movie_times'=>0]);
                    foreach($movieTimesById as $mt) {
                        $mt->update(['is_active'=> 0]);
                    }

                     $results = Movie::all()->where('active',1);
                     app('App\Http\Controllers\ReportController')->store($request->id);
                }  else {
                    //Only update the movie 'cause doesn't exist any movieTime with this id
                    $movieM->update(['active'=>0]);
                     $movieM->update(['has_movie_times'=>0]);
                     $results = Movie::all()->where('active',1);
                     app('App\Http\Controllers\ReportController')->store($request->id);
                }
           
            //if it's empty, only desactivate the movie
            } else {
                $movieM->update(['active'=>0]);
                 $movieM->update(['has_movie_times'=>0]);
                $results = Movie::all()->where('active',1);
                app('App\Http\Controllers\ReportController')->store($request->id);
            }
        } else {
            return response()-json(
                ['msg' => 'No hay nada'], 404
            );
        }
                    // $reports = Report::all();
                    //    foreach($reports as $report) {
                    //        $report->movie;
                    //    }
                        //return response()->json($reports);
                    //    return view('reports.index', compact('reports'));
        //Al terminar todo eso, deberia ejecutar el metodo ver reporte por pelicula
         
                $reports = Report::all();
                foreach($reports as $report) {
                    if($report->movie_id == $request->id) {
                        $report->movie;
                        return view('reports.show', compact('report'));
                    } 
                }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function destroy(Movie $movie)
    {
        //
    }
}
