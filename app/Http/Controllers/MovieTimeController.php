<?php

namespace App\Http\Controllers;

use App\MovieTime;
use App\Movie;
use Illuminate\Http\Request;
use Carbon\Carbon;

class MovieTimeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

      
            $movieTimes = MovieTime::orderBy('movie_time_date')->where('is_active', 1)->get();
            foreach($movieTimes as $movieTime) {
                $movies[] = $movieTime->movie;
                $movieTime->room;
                $movieTime->displayType;
                foreach($movies as $movie) {
                        $movie->genre;
                        $movie->rating;
                    }
                }
                return response()->json($movieTimes);      
       

         
            
    }

    public function filterByDate(Request $request) {
         if(isset($request->date)) {
            $date = Carbon::parse($request->date);
            $movieTimes = MovieTime::where('is_active',1);
            $movieTimes->where('movie_time_date',$date->format('y-m-d'))->get();     
        }
        return view('movieTimes.dates',compact('movieTimes', 'date'));
    }

    public function indexView() {

        return view('movieTimes.movieTimes');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('movieTimes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $movieTime = MovieTime::create($request->all());
        Movie::find($request->movie_id)->update(['has_movie_times'=>1]);
        return response()->json($movieTime, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MovieTime  $movieTime
     * @return \Illuminate\Http\Response
     */
   public function show($id)
    {
         $count = MovieTime::where('id', $id)->count();
        
        if($count ==1){
            $movieTime = MovieTime::find($id);
                return response()->json($movieTime);
        } else {
            return response()->json(
                
                ['msg' => 'Función no encontrada'],
                404
            );
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MovieTime  $movieTime
     * @return \Illuminate\Http\Response
     */
    public function edit(MovieTime $movieTime)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MovieTime  $movieTime
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
      /*  $count = MovieTime::where('id', $id)->count();

        if($count == 1) {
            MovieTime::find($id)->update(
                ['is_active'=>0]
            );
            return response()->json(
                ['msg' => 'yes'], 200
            );
        } else {
            return response()-json(
                ['msg' => 'No hay nada'], 404
            );
        }*/
        $movieTime = MovieTime::find($id);
            return response()->json(
               $movieTime, 200
            );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MovieTime  $movieTime
     * @return \Illuminate\Http\Response
     */
    public function destroy(MovieTime $movieTime)
    {
        //
    }
}
