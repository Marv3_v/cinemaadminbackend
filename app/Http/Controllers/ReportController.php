<?php

namespace App\Http\Controllers;
use App\Movie;
use App\MovieTime;
use App\Report;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $reports = Report::all();
       foreach($reports as $report) {
        $report->movie;
       }
       //return response()->json($reports);
       return view('reports.index', compact('reports'));
    }

    public function getReport() {
        $totalTickets;
        $exhibitor;
        $distributor;
        $totalMoney;
        $reports = Report::all();
        foreach($reports as $report){
            global $totalTickets;
            global $exhibitor;
            global $totalMoney;
            $totalTickets += $report->tickets_2d + $report->tickets_3d;
            $totalMoney += $report->box_office;
            $exhibitor += $report->film_exhibitor_utilities;
        }
        return view('admin', compact('totalTickets', 'exhibitor','totalMoney'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($id)
    {
        //$reports = Movie::all()->where('active', 0);
        //$movie = Movie::find($request->id);
            //global variables
            $total2d = 0;
            $total3d = 0;
            $ticketsByMovie2d = 0;
            $ticketsByMovie3d = 0;
            $ticketsSold;
            // getting the movie itself
            $movie = Movie::find($id);
            $movieTimes = $movie->movieTimes;
            $movieTimes->where('has_tickets', 1);


            //movieTimes only in 2d
            $movieTimes2d = $movieTimes->where('display_type_id',1);
            //movieTimes only in 3d
            $movieTimes3d = $movieTimes->where('display_type_id',2);
            
            //las funciones en 3d y 2d no estan vacias
            if($movieTimes2d!="" && $movieTimes3d !="") {   
              
                foreach($movieTimes2d as $movieT) {
                    $tByM = $movieT->tickets;
                    global $ticketsByMovie2d;
                    $ticketsByMovie2d = $ticketsByMovie2d + $tByM->count();
                    global $total2d;
                    $total2d =  $movieT->total * $ticketsByMovie2d;               
               }
               foreach($movieTimes3d as $movieT) {
                    $tByM = $movieT->tickets;
                    global $ticketsByMovie3d;
                    $ticketsByMovie3d = $ticketsByMovie3d + $tByM->count();
                    global $total3d;
                    $total3d =  $movieT->total * $ticketsByMovie3d;        
            }
            //Si solo hay funciones en 2d
        } else if($movieTimes2d!="" && $movieTimes3d ==""){
           
            foreach($movieTimes2d as $movieT) {
                    $tByM = $movieT->tickets;
                    global $ticketsByMovie2d;
                    $ticketsByMovie2d = $ticketsByMovie2d + $tByM->count();
                    global $total2d;
                    $total2d =  $movieT->total * $ticketsByMovie2d;               
               }
               //Si solo hay funciones en 3d
        } else if($movieTimes2d=="" && $movieTimes3d !="") {
            
            foreach($movieTimes3d as $movieT) {
                    $tByM = $movieT->tickets;
                    global $ticketsByMovie3d;
                    $ticketsByMovie3d = $ticketsByMovie3d + $tByM->count();
                    global $total3d;
                    $total3d =  $movieT->total * $ticketsByMovie3d;          
            }
            //echo "solo 3d";
        }
         //Taquilla
                $boxOffice = $total2d + $total3d;
                //iva
                $iva = $boxOffice * 0.12;
                //Distributor 46.3 %
                $d_utilities = $boxOffice * 0.463;
                //film_exhibitor 38.7 %
                $e_utilities = $boxOffice * 0.387;
                //Derechos de autor 3%
                $copyRight = $boxOffice * 0.03;
                //
                global $ticketsSold;
            
                $ticketsSold = $ticketsByMovie3d + $ticketsByMovie2d;
                $report = new Report;
                $report->ticket_amount = $ticketsSold;
                $report->tickets_2d = $ticketsByMovie2d;
                $report->tickets_3d = $ticketsByMovie3d;
                $report->box_office = $boxOffice;
                $report->iva_tax = $iva;
                $report->copyright = $copyRight;
                $report->distributor_utilities = $d_utilities;
                $report->film_exhibitor_utilities = $e_utilities;
                $report->movie_id = $id;
                $report->save();
                //Movie::find($id)->update(['has_reports'=>1]);
                $boxOffice = 0;
                $iva = 0;
                $ticketsSold = 0;
                $ticketsByMovie3d = 0;
                $ticketsByMovie2d = 0;
                $e_utilities = 0;
                $d_utilities = 0;
                $total3d = 0;
                $total2d = 0;
                
               
                    //return response()->json($reports);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $report = Report::find($id);
        $report->movie;
        return view('reports.show', compact('report'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
