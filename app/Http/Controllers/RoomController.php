<?php

namespace App\Http\Controllers;

use App\Room;
use Illuminate\Http\Request;
use Spatie\Searchable\Search;

class RoomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
         if($request->has('queryRoom')) { 
            
            $results = (new Search())
                ->registerModel(Room::class, 'number')
                ->search($request->input('queryRoom'))
                ->pluck('searchable');
                return response()->json($results);
        } else {
            $results = Room::all();
            
        }
            return response()->json($results);
    }

    public function indexView() {
            $rooms = Room::all();
            return view('rooms.index', compact('rooms'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('rooms.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateRoom = $request->validate([
            'number' =>
                array(
                   'required',
                   'regex:/^[a-zA-Z]/',
                    'max:80'
                ),
            'chairs_amount' =>
                array(
                    'required'
                )
       ]); 
        Room::create($validateRoom);
        return $this->indexView();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $count = Room::where('id', $id)->count();
        
        if($count ==1){
            $room = Room::find($id);
                return response()->json($room);
        } else {
            return response()->json(
                
                ['msg' => 'Sala no encontrada'],
                404
            );
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function edit(Room $room)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $id)
    {
        $count = Room::where('id', $id)->count();

        if($count == 1) {
            Room::find($id)->update(
                ['number'=>$request->updated]
            );
            return response()->json(
                ['msg' => 'yes'], 200
            );
        } else {
            return response()-json(
                ['msg' => 'No hay nada'], 404
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $room = Room::where('id', $id)->first()->get();
        if (is_null($room)) {
            return response()->json(['msg'=>'falló'], 404);
        }else{
            
            Room::find($id)->update(['deleted'=>1]);
            return response()->json([
                'msg'=>'Actualizado'
            ]);
        }
    }
}
