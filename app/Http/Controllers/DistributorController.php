<?php

namespace App\Http\Controllers;

use App\Distributor;
use Illuminate\Http\Request;
use Spatie\Searchable\Search;

class DistributorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if($request->has('queryD')) {

            $results = (new Search())
            ->registerModel(Distributor::class, 'name')
            ->search($request->input('queryD'))
            ->pluck('searchable');
        } else {
            return response()->json(
            Distributor::all()->where('deleted',0)
        );
        }

        return response()->json($results);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateDistri = $request->validate([
            'name' =>
                array(
                   'required',
                   'regex:/^[a-zA-Z]+$/u',
                    'max:80'
                )
       ]);
   $distri = Distributor::create($validateDistri);
   return response()->json($distri, 202);
   
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Distributor  $distributor
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $count = Distributor::where('id', $id)->count();
        
        if($count ==1){
            $distributor = Distributor::find($id);
                return response()->json($distributor);
        } else {
            return response()->json(
                
                ['msg' => 'Distribuidor no encontrada'],
                404
            );
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Distributor  $distributor
     * @return \Illuminate\Http\Response
     */
    public function edit(Distributor $distributor)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Distributor  $distributor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $count = Distributor::where('id', $id)->count();

        if($count == 1) {
            Distributor::find($id)->update(
                ['name'=>$request->updated]
            );
            return response()->json(
                ['msg' => 'yes'], 200
            );
        } else {
            return response()-json(
                ['msg' => 'No hay nada'], 404
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Distributor  $distributor
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $distributor = Distributor::where('id', $id)->first()->get();
        if (is_null($distributor)) {
            return response()->json(['msg'=>'falló'], 404);
        }else{
            
            Distributor::find($id)->update(['deleted'=>1]);
            return response()->json([
                'msg'=>'Actualizado'
            ]);
        }
    }
}
