<?php

namespace App\Http\Controllers;

use App\Rating;
use Illuminate\Http\Request;
use Spatie\Searchable\Search;

class RatingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->has('queryR')) {
              $results = (new Search())
            ->registerModel(Rating::class, 'rating')
            ->search($request->input('queryR'))
            ->pluck('searchable');
        } else {
            return response()->json(
            Rating::all()->where('deleted',0)
            );
        }

        return response()->json($results);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateRating = $request->validate([
            'rating' =>
                array(
                   'required',
                   'regex:/^[a-zA-Z]+$/u',
                    'max:80'
                )
       ]);
   $rating = Rating::create($validateRating);
   return response()->json($rating, 202);
   
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Rating  $rating
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $count = Rating::where('id', $id)->count();
        
        if($count ==1){
            $rating = Rating::find($id);
                return response()->json($rating);
        } else {
            return response()->json(
                
                ['msg' => 'Clasificación no encontrada'],
                404
            );
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Rating  $rating
     * @return \Illuminate\Http\Response
     */
    public function edit(Rating $rating)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Rating  $rating
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $count = Rating::where('id', $id)->count();

        if($count == 1) {
            Rating::find($id)->update(
                ['rating'=>$request->updated]
            );
            return response()->json(
                ['msg' => 'yes'], 200
            );
        } else {
            return response()-json(
                ['msg' => 'No hay nada'], 404
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Rating  $rating
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $rating = Rating::where('id', $id)->first()->get();
        if (is_null($rating)) {
            return response()->json(['msg'=>'falló'], 404);
        }else{
            
            Rating::find($id)->update(['deleted'=>1]);
            return response()->json([
                'msg'=>'Actualizado'
            ]);
        }
    }
}
