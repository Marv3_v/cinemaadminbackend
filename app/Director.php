<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;

class Director extends Model implements Searchable
{
    protected $fillable = ['fullname', 'deleted'];

    public function movies() {
        return $this->hasMany('App\Movie');
    }

    public function getSearchResult(): SearchResult
    {
        $url = route('directors.show', $this->id);
        return new SearchResult($this, $this->fullname, $url);
    }
}
