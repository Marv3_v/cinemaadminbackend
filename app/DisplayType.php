<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;

class DisplayType extends Model implements Searchable
{
    protected $fillable = ['type'];

    public function getSearchResult(): SearchResult
    {
        $url = route('displayTypes.show', $this->id);
        return new SearchResult($this, $this->type, $url);
    }
    
    public function movieTime() {
        return $this->hasMany('App\MovieTime');
    }
}
