<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;

class Room extends Model implements Searchable
{
    protected $fillable = [
            'number',
            'chairs_amount'
        ];

     public function getSearchResult(): SearchResult
    {
        $url = route('rooms.show', $this->id);
        return new SearchResult($this, $this->number, $url);
    }

    public function movieTime() {
        return $this->hasMany('App\MovieTime');
    }
}
