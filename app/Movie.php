<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;

class Movie extends Model implements Searchable
{
    protected $fillable = [
            'title',
            'synopsis',
            'poster',
            'year',
            'duration',
            'active',
            'has_movie_times',
            'has_tickets',
            'director_id',
            'distributor_id',
            'genre_id',
            'rating_id',
        ];

    public function getSearchResult(): SearchResult
    {
        $url = route('movies.show', $this->id);
        return new SearchResult($this, $this->title, $url);
    }

    public function director() {
        return $this->belongsTo('App\Director');
    }
    
    public function distributor() {
        return $this->belongsTo('App\Distributor');
    }
    
    public function genre() {
        return $this->belongsTo('App\Genre');
    }

    public function rating() {
        return $this->belongsTo('App\Rating');
    }

    public function movieTimes() {
        return $this->hasMany('App\MovieTime');
    }

    public function report(){
        return $this->hasOne('App\Report');
    }
}
