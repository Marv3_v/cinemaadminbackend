<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;

class Distributor extends Model implements Searchable
{
    protected $fillable = ['name', 'deleted'];

    public function movies() {
        return $this->hasMany('App\Movie');
    }

     public function getSearchResult(): SearchResult
    {
        $url = route('distributors.show', $this->id);
        return new SearchResult($this, $this->name, $url);
    }
}
